package plugins.adufour.activecontours;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;

import icy.gui.viewer.Viewer;
import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageUtil;
import icy.main.Icy;
import icy.math.ArrayMath;
import icy.math.Scaler;
import icy.painter.Overlay;
import icy.painter.Overlay.OverlayPriority;
import icy.roi.BooleanMask2D;
import icy.roi.BooleanMask3D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.roi.ROIUtil;
import icy.sequence.DimensionId;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.swimmingPool.SwimmingObject;
import icy.system.IcyHandledException;
import icy.system.SystemUtil;
import icy.system.thread.Processor;
import icy.system.thread.ThreadUtil;
import icy.type.DataType;
import icy.type.collection.CollectionUtil;
import icy.type.rectangle.Rectangle3D;
import icy.util.OMEUtil;
import icy.util.ShapeUtil.BooleanOperator;
import icy.util.StringUtil;
import plugins.adufour.activecontours.SlidingWindow.Operation;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzButton;
import plugins.adufour.ezplug.EzException;
import plugins.adufour.ezplug.EzGUI;
import plugins.adufour.ezplug.EzGroup;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVar;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarDimensionPicker;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarListener;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.filtering.Convolution1D;
import plugins.adufour.filtering.Kernels1D;
import plugins.adufour.hierarchicalkmeans.HKMeans;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackManager;
import plugins.fab.trackmanager.TrackSegment;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi2d.ROI2DPolygon;
import plugins.kernel.roi.roi2d.ROI2DRectangle;
import plugins.kernel.roi.roi3d.ROI3DStack;
import plugins.nchenouard.spot.Detection;
import vtk.vtkObjectBase;

public class ActiveContours extends EzPlug implements EzStoppable, Block
{
    public static final String CONTOUR_BASE_NAME = "Contour #";
    public static final String CONTOUR_ID = "contourId";

    private class LocalRegionStatisticsComputer implements Callable<Object>
    {
        final ActiveContour contour;
        final boolean maskBased;

        public LocalRegionStatisticsComputer(final ActiveContour contour, final boolean maskBased)
        {
            this.contour = contour;
            this.maskBased = maskBased;
        }

        @Override
        public Object call() throws InterruptedException
        {
            try
            {
                final double cin = contour.computeAverageIntensity(contour instanceof Mesh3D ? regionData : regionDataSummed,
                        maskBased ? contourMask_buffer : null);
                region_cin.put(trackGroup.getValue().getTrackSegmentWithDetection(contour), cin);
            }
            catch (TopologyException topo)
            {
                System.err.println("Removing a contour. Reason: " + topo.getMessage());
                allContoursAtTimeT.remove(contour);
                evolvingContoursAtTimeT.remove(contour);
            }
            catch (UnsupportedOperationException ex)
            {
                System.err.println("Error while computing contour statistics: " + ex.getMessage());
            }

            return null;
        }
    }

    private class LocalBackgroundStatisticsComputer implements Callable<Object>
    {
        final ActiveContour contour;

        public LocalBackgroundStatisticsComputer(final ActiveContour contour)
        {
            this.contour = contour;
        }

        @Override
        public Object call()
        {
            final TrackSegment segment = trackGroup.getValue().getTrackSegmentWithDetection(contour);

            final double cout = contour.computeBackgroundIntensity(regionData, contourMask_buffer);

            region_cout.put(segment, cout);

            return null;
        }
    }

    private class ContourInitializer implements Callable<Object>
    {
        final ROI roi;
        final int z;
        final int t;
        final Tuple3d pixelSize;
        final int convWinSize;
        final List<TrackSegment> activeTracks;
        final List<TrackSegment> endedTracks;

        public ContourInitializer(final ROI roi, final int z, final int t, final Tuple3d pixelSize, final int convWinSize,
                final List<TrackSegment> activeTracks, final List<TrackSegment> justEndedTracks)
        {
            super();

            this.roi = roi;
            this.z = z;
            this.t = t;
            this.pixelSize = pixelSize;
            this.convWinSize = convWinSize;
            this.activeTracks = activeTracks;
            this.endedTracks = justEndedTracks;
        }

        // test if the object is colliding an existing contour or if we need to discard
        // it for other reason
        private boolean colliding() throws UnsupportedOperationException, InterruptedException
        {
            // // image bounds
            // final Rectangle imageBounds = inputData.getBounds2D();
            // // minus one (to detect object on border)
            // imageBounds.x++;
            // imageBounds.y++;
            // imageBounds.width -= 2;
            // imageBounds.height -= 2;

            // test if object is intersecting with current active contours
            for (final TrackSegment segment : activeTracks)
            {
                // get contour for this active track
                final ActiveContour contour = (ActiveContour) segment.getLastDetection();
                // get ROI from contour
                final ROI contourROI = contour.toROI(ROIType.POLYGON, null);

                // object is intersecting contour ? --> discard
                if (roi.intersects(contourROI))
                    return true;
            }

            return false;
        }

        @Override
        public Object call() throws InterruptedException
        {
            // object colliding active contours ? --> discard it
            if (colliding())
                return null;

            // get contour for the new object
            final ActiveContour contour = getContourOf(roi, z, t, pixelSize, convWinSize);
            // error creating contour
            if (contour == null)
                return null;

            // does it overlap with a track that terminates in the previous frame?
            for (final TrackSegment track : endedTracks)
            {
                final ActiveContour previousContour = (ActiveContour) track.getLastDetection();
                // get ROI from contour
                final ROI previousContourROI = previousContour.toROI(ROIType.POLYGON, null);

                // object is intersecting previous contour ?
                if (roi.intersects(previousContourROI))
                {
                    System.out.println("Found link at time " + t + ", position (" + contour.getX() + ";" + contour.getY() + ")");
                    // add contour to the track
                    track.addDetection(contour);
                    // done (no new track)
                    return null;
                }
            }

            // need to create a new track
            final TrackSegment result = new TrackSegment();
            // add contour to it
            result.addDetection(contour);

            // and return the new created track
            return result;
        }
    }

    private class ContourDuplicator implements Callable<Object>
    {
        final TrackSegment segment;
        final int t;
        final int convWinSize;

        public ContourDuplicator(final TrackSegment segment, final int t, final int convWinSize)
        {
            super();

            this.segment = segment;
            this.t = t;
            this.convWinSize = convWinSize;
        }

        @Override
        public Object call()
        {
            Detection detection;

            detection = segment.getDetectionAtTime(t);
            // already have detection for that time point (shouldn't be the case) ? --> exit
            if (detection != null)
                return detection;

            detection = segment.getDetectionAtTime(t - 1);
            // no detection for previous time point ? nothing to do
            if (detection == null)
                return null;

            final ActiveContour previousContour = (ActiveContour) detection;
            final ActiveContour currentContour = previousContour.clone();

            currentContour.convergence.setSize(convWinSize);
            currentContour.setT(t);

            segment.addDetection(currentContour);

            return currentContour;
        }
    }

    private final double EPSILON = 0.0000001;

    private final EzVarBoolean showAdvancedOptions = new EzVarBoolean("Show advanced options", false);

    public final EzVarSequence input = new EzVarSequence("Input");
    private Sequence inputData;

    public final EzVarDouble regul_weight = new EzVarDouble("Contour smoothness", 0.05, 0, 1.0, 0.01);

    public final EzGroup edge = new EzGroup("Find bright/dark edges");
    public final EzVarDimensionPicker edge_c = new EzVarDimensionPicker("Find edges in channel", DimensionId.C, input);
    public final EzVarDouble edge_weight = new EzVarDouble("Edge weight", 0, -1, 1, 0.1);

    public final EzGroup region = new EzGroup("Find homogeneous intensity areas");
    public final EzVarDimensionPicker region_c = new EzVarDimensionPicker("Find regions in channel", DimensionId.C, input);
    public final EzVarDouble region_weight = new EzVarDouble("Region weight", 1.0, 0.0, 1.0, 0.1);
    public final EzVarDouble region_sensitivity = new EzVarDouble("Region sensitivity", 1.0, 0.2, 5.0, 0.1);
    public final EzVarBoolean region_localise = new EzVarBoolean("Variable background", false);

    public final EzVarDouble balloon_weight = new EzVarDouble("Contour inflation", 0, -0.5, 0.5, 0.001);

    public final EzVarDouble axis_weight = new EzVarDouble("Axis constraint", 0, 0.0, 1, 0.1);

    public final EzVarBoolean coupling_flag = new EzVarBoolean("Multi-contour coupling", true);

    public final EzGroup evolution = new EzGroup("Evolution parameters");
    public final EzVarSequence evolution_bounds = new EzVarSequence("Bound field to ROI of");
    public final EzVarDouble contour_resolution = new EzVarDouble("Contour sampling", 2, 0.1, 10000.0, 0.1);
    public final EzVarDouble contour_timeStep = new EzVarDouble("Evolution time step", 0.1, 0.1, 10, 0.01);
    public final EzVarInteger convergence_winSize = new EzVarInteger("Convergence window size", 50, 10, 10000, 10);
    public final EzVarEnum<Operation> convergence_operation = new EzVarEnum<SlidingWindow.Operation>("Convergence operation", Operation.values(),
            Operation.VAR_COEFF);
    public final EzVarDouble convergence_criterion = new EzVarDouble("Convergence criterion", 0.001, 0, 1, 0.0001);
    public final EzVarInteger convergence_nbIter = new EzVarInteger("Max. iterations", 100000, 100, 100000, 1000);

    public enum ExportROI
    {
        NO, ON_INPUT, ON_NEW_IMAGE, AS_LABELS
    }

    public enum ROIType
    {
        AREA(ROI2DArea.class), POLYGON(ROI2DPolygon.class);

        final Class<? extends ROI> clazz;

        private ROIType(final Class<? extends ROI> clazz)
        {
            this.clazz = clazz;
        }
    }

    public final EzVarEnum<ExportROI> output_rois = new EzVarEnum<ExportROI>("Export ROI", ExportROI.values(), ExportROI.NO);
    public final EzVarEnum<ROIType> output_roiType = new EzVarEnum<ROIType>("Type of ROI", ROIType.values(), ROIType.AREA);
    private final VarSequence output_labels = new VarSequence("Labels", null);

    public final EzVarBoolean tracking = new EzVarBoolean("Track objects over time", false);

    public final EzVarDouble division_sensitivity = new EzVarDouble("Division sensitivity", 0, 0, 2, 0.1);

    public final EzVarBoolean tracking_newObjects = new EzVarBoolean("Watch entering objects", false);

    private final HashMap<TrackSegment, Double> volumes = new HashMap<TrackSegment, Double>();
    public final EzVarBoolean volume_constraint = new EzVarBoolean("Volume constraint", false);
    public final EzVarDouble volume_weight = new EzVarDouble("Volume weight", 0.01, 0, 1, 0.001);

    public final EzButton showTrackManager;

    private Sequence edgeData;
    private Sequence regionData;
    private Sequence regionDataSummed;

    private BooleanMask3D contourMask_buffer;

    HashMap<TrackSegment, Double> region_cin = new HashMap<TrackSegment, Double>(0);
    HashMap<TrackSegment, Double> region_cout = new HashMap<TrackSegment, Double>(0);

    public final VarROIArray roiInput = new VarROIArray("input ROI");
    public final VarROIArray roiOutput = new VarROIArray("Regions of interest");

    private boolean globalStop;

    Var<TrackGroup> trackGroup = new Var<TrackGroup>("Tracks", TrackGroup.class);

    /**
     * All contours present on the current time point
     */
    private final HashSet<ActiveContour> allContoursAtTimeT = new HashSet<ActiveContour>();

    /**
     * Set of contours that have not yet converged on the current time point
     */
    private final HashSet<ActiveContour> evolvingContoursAtTimeT = new HashSet<ActiveContour>();

    private ActiveContoursOverlay overlay;

    private final Processor multiThreadService = new Processor(SystemUtil.getNumberOfCPUs());

    private long lastVtkGCTime = 0L;

    public ActiveContours()
    {
        super();

        multiThreadService.setThreadName("Active Contours");

        showTrackManager = new EzButton("Send to track manager", new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                ThreadUtil.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if (trackGroup.getValue() == null)
                            return;
                        if (trackGroup.getValue().getTrackSegmentList().isEmpty())
                            return;

                        Icy.getMainInterface().getSwimmingPool().add(new SwimmingObject(trackGroup.getValue()));
                        final TrackManager tm = new TrackManager();
                        tm.reOrganize();
                        tm.setDisplaySequence(inputData);
                    }
                });
            }
        });
    }

    public TrackGroup getTrackGroup()
    {
        return trackGroup.getValue();
    }

    @Override
    public void initialize()
    {
        addEzComponent(showAdvancedOptions);
        addEzComponent(input);

        // edge
        edge.setToolTipText("Sets the contour(s) to follow image intensity gradients");
        edge_weight.setToolTipText("Negative (resp. positive) weight pushes contours toward decreasing (resp. increasing) intensities");
        edge.add(edge_c, edge_weight);
        addEzComponent(edge);

        // region
        region.setToolTipText("Sets the contour(s) to isolate homogeneous intensity regions");
        region_weight.setToolTipText("Set to 0 to deactivate this parameter");
        region_sensitivity.setToolTipText("Increase this value to be more sensitive to dim objects (default: 1)");
        region_localise.setToolTipText("Check this box if the image background is noisy or non-homogeneous");
        showAdvancedOptions.addVisibilityTriggerTo(region_sensitivity, true);
        // don't show local means (for now)
        showAdvancedOptions.addVisibilityTriggerTo(region_localise, true);
        region.add(region_c, region_weight, region_sensitivity);// , region_localise);
        addEzComponent(region);

        // coupling
        coupling_flag.setToolTipText("Prevents multiple contours from overlapping");
        showAdvancedOptions.addVisibilityTriggerTo(coupling_flag, true);
        addEzComponent(coupling_flag);

        // regul
        regul_weight.setToolTipText("Higher values result in a smoother contour, but may also slow its growth");
        addEzComponent(regul_weight);
        // balloon force
        balloon_weight.setToolTipText("Positive (resp. negative) values will inflate (resp. deflate) the contour");
        addEzComponent(balloon_weight);

        // axis contraint
        axis_weight.setToolTipText("Higher values restrict the evolution along the principal axis");
        addEzComponent(axis_weight);

        // sensitivity to detect division ahead of time
        division_sensitivity.setToolTipText("Increase the sensitivity to cell division");
        showAdvancedOptions.addVisibilityTriggerTo(division_sensitivity, true);
        addEzComponent(division_sensitivity);

        // contour
        contour_resolution.setToolTipText("Sets the contour(s) precision as the distance (in pixels) between control points");

        contour_timeStep.setToolTipText("Defines the evolution speed (warning: keep a low value to avoid vibration effects)");

        convergence_winSize.setToolTipText("Defines over how many iterations the algorithm should check for convergence");
        showAdvancedOptions.addVisibilityTriggerTo(convergence_winSize, true);

        convergence_operation.setToolTipText("Defines the operation used to detect convergence");
        showAdvancedOptions.addVisibilityTriggerTo(convergence_operation, true);

        convergence_criterion.setToolTipText("Defines the value of the criterion used to detect convergence");

        convergence_nbIter.setToolTipText("Defines the absolute number of iterations to use in case the contour does not converge automatically");
        showAdvancedOptions.addVisibilityTriggerTo(convergence_nbIter, true);

        evolution_bounds.setNoSequenceSelection();
        evolution_bounds.setToolTipText("Bounds the evolution of the contour to all ROI of the given sequence (select \"No sequence\" to deactivate)");
        showAdvancedOptions.addVisibilityTriggerTo(evolution_bounds, true);

        evolution.add(evolution_bounds, contour_resolution, contour_timeStep, convergence_winSize, convergence_operation, convergence_criterion,
                convergence_nbIter);
        addEzComponent(evolution);

        // output
        output_rois.setToolTipText("Select whether and where to export the contours as ROI for further quantification");
        addEzComponent(output_rois);
        output_roiType.setToolTipText("Select the type of ROI to export");
        addEzComponent(output_roiType);
        output_rois.addVisibilityTriggerTo(output_roiType, ExportROI.ON_INPUT, ExportROI.ON_NEW_IMAGE);

        // tracking
        tracking.setToolTipText("Track objects over time");
        addEzComponent(tracking);

        addEzComponent(tracking_newObjects);
        tracking.addVisibilityTriggerTo(tracking_newObjects, true);
        addEzComponent(volume_constraint);
        tracking.addVisibilityTriggerTo(volume_constraint, true);
        addEzComponent(volume_weight);
        volume_constraint.addVisibilityTriggerTo(volume_weight, true);

        addEzComponent(showTrackManager);

        setTimeDisplay(true);

        volume_constraint.setValue(Boolean.FALSE);
    }

    @Override
    public void loadParameters(final File file)
    {
        super.loadParameters(file);

        volume_constraint.setValue(Boolean.FALSE);
    }

    @Override
    public void execute()
    {
        output_labels.setValue(null);
        volumes.clear();
        roiOutput.setValue(null);
        inputData = input.getValue(true);

        final Viewer viewer = inputData.getFirstViewer();

        globalStop = false;

        final int startT = (viewer == null) ? 0 : viewer.getPositionT();
        final int endT = tracking.getValue().booleanValue() ? inputData.getSizeT() - 1 : startT;

        // Need to do it before clearing track group to remove old painters (Stephane)
        if (overlay != null)
            overlay.remove();

        // Reset (if any) and (re)create a new track group for the current run
        TrackGroup tracks = trackGroup.getValue(false);
        if (tracks != null)
            tracks.clearAllTrackSegment();
        tracks = new TrackGroup(inputData);
        tracks.setDescription("Active contours (" + new Date().toString() + ")");
        trackGroup.setValue(tracks);

        // initialize the mask buffer once (used to calculate average intensities
        // inside/outside)
        final Rectangle3D.Integer bounds3d = inputData.getBounds5D().toRectangle3D().toInteger();
        final Rectangle bounds2d = inputData.getBounds2D();
        final BooleanMask2D[] maskSlices = new BooleanMask2D[bounds3d.sizeZ];

        for (int z = 0; z < bounds3d.sizeZ; z++)
            maskSlices[z] = new BooleanMask2D(bounds2d, new boolean[bounds3d.sizeX * bounds3d.sizeY]);

        contourMask_buffer = new BooleanMask3D(bounds3d, maskSlices);

        if (!Icy.getMainInterface().isHeadLess())
        {
            // replace any ActiveContours Painter object on the sequence by ours
            for (final Overlay existingOverlay : inputData.getOverlays())
            {
                if (existingOverlay instanceof ActiveContoursOverlay)
                    existingOverlay.remove();
            }

            overlay = new ActiveContoursOverlay(tracks);
            overlay.setPriority(OverlayPriority.TOPMOST);
            inputData.addOverlay(overlay);
        }

        // interactive mode ? --> use ROIs from sequence
        if (getUI() != null)
            roiInput.setValue(new ROI[0]);

        // initialize bounding field for once
        ROI field = null;
        final Sequence boundSource = evolution_bounds.getValue();

        if (boundSource != null)
        {
            final List<ROI> rois = boundSource.getROIs();

            if (rois.size() > 0)
            {
                try
                {
                    field = ROIUtil.merge(rois, BooleanOperator.OR);
                }
                catch (final UnsupportedOperationException e)
                {
                    throw new VarException(evolution_bounds.getVariable(), "Cannot compute the evolution bounds: " + e.getMessage()
                            + "\nIf you are not sure how to fix this, change this parameter to \"No Sequence\"");
                }
                catch (InterruptedException e)
                {
                    throw new VarException(evolution_bounds.getVariable(), "Interrupted Active Contours process...");
                }
            }
        }

        // no specific field ? limit to sequence bounds
        if (field == null)

        {
            if (inputData.getSizeZ() == 1)
                field = new ROI2DRectangle(0, 0, inputData.getWidth(), inputData.getHeight());
            else
            {
                final ROI3DStack<ROI2DRectangle> field3D = new ROI3DStack<ROI2DRectangle>(ROI2DRectangle.class);

                for (int z = 0; z < inputData.getSizeZ() - 1; z++)
                    field3D.setSlice(z, new ROI2DRectangle(0, 0, inputData.getWidth(), inputData.getHeight()));

                field = field3D;
            }
        }

        for (int t = startT; t <= endT; t++)
        {
            int iteration;

            // if sizeT changed during AC processing (i already did it to shorthen
            // processing time :p)
            if (t >= inputData.getSizeT())
                break;

            if (isHeadLess())
                System.out.println("Processing frame #" + t);

            // change time point on viewer
            if (viewer != null)
                viewer.setPositionT(t);

            if (isHeadLess())
                System.out.println("=> preparing image data...");
            try
            {
                initData(t);

                if (isHeadLess())
                    System.out.println("=> initializing current contours...");
                initCurrentContours(t);

                if (isHeadLess())
                    System.out.println("=> evolving current contours...");
                // evolve current contours
                iteration = evolveContours(t, field);
                if (iteration > 0)
                    System.out.println("[Active Contours] converged current contours on frame " + t + " in " + iteration + " iterations");
            }
            catch (InterruptedException e)
            {
                // so it will end nicely
                Thread.currentThread().interrupt();
            }

            if (Thread.currentThread().isInterrupted())
                break;

            if (isHeadLess())
                System.out.println("=> adding new contours...");
            try
            {
                addNewContours(t);
            }
            catch (InterruptedException e)
            {
                // so it will end nicely
                Thread.currentThread().interrupt();
            }

            if (Thread.currentThread().isInterrupted())
                break;

            try
            {
                if (isHeadLess())
                    System.out.println("=> evolving new contours...");
                // evolve new contours
                iteration = evolveContours(t, field);

                // no more contours for this time point ?
                if (iteration == -1)
                {
                    // detection of new objects disable ?
                    if (!tracking_newObjects.getValue().booleanValue() || (inputData.getSizeZ() != 1))
                    {
                        // no ROIs to work with in future ? --> we can stop process here
                        if (!hasFutureRois(t))
                        {
                            storeResult(t);
                            break;
                        }
                    }
                }

                // log info
                if (iteration > 0)
                    System.out.println("[Active Contours] converged new contours on frame " + t + " in " + iteration + " iterations");

                // store detections and results
                storeResult(t);
            }
            catch (InterruptedException e)
            {
                // interrupt nicely
                Thread.currentThread().interrupt();
            }

            if (Thread.currentThread().isInterrupted())
                break;

            if (globalStop)
                break;
        }

        int maxId = 0;
        // get maximum id
        for (final ROI roi : roiOutput.getValue())
        {
            try
            {
                // get object id
                final int contourId = Integer.parseInt(roi.getProperty(CONTOUR_ID));
                if (contourId > maxId)
                    maxId = contourId;
            }
            catch (final Exception e)
            {
                // ignore
            }
        }

        // then fix ROI names
        final int nbPaddingDigits = (int) Math.floor(Math.log10(maxId));
        for (final ROI roi : roiOutput.getValue())
        {
            try
            {
                // get object id
                final int contourId = Integer.parseInt(roi.getProperty(CONTOUR_ID));
                final String roiName = roi.getName();

                if (StringUtil.isEmpty(roiName))
                    roi.setName(CONTOUR_BASE_NAME + StringUtil.toString(contourId, nbPaddingDigits));
                else
                    roi.setName(CONTOUR_BASE_NAME + StringUtil.toString(contourId, nbPaddingDigits) + " (" + roiName + ")");
            }
            catch (final Exception e)
            {
                // ignore
            }
        }

        if (getUI() != null)
        {
            Sequence out = inputData;

            switch (output_rois.getValue())
            {
                case ON_NEW_IMAGE:
                    try
                    {
                        out = SequenceUtil.getCopy(inputData);
                        out.setName(inputData.getName() + " + Active contours");
                    }
                    catch (InterruptedException e)
                    {
                        throw new VarException(output_rois.getVariable(), "Interrupted output sequence creation...");
                    }
                    //$FALL-THROUGH$
                case ON_INPUT:
                    for (final ROI roi : roiOutput.getValue())
                        out.addROI(roi, false);
                    if (out != inputData)
                        addSequence(out);
                    break;
                case AS_LABELS:
                    addSequence(output_labels.getValue());
                    break;
                default:
            }
        }

        // remove the painter after processing if an export was required
        if (output_rois.getValue() != ExportROI.NO && overlay != null)
            overlay.remove();

        multiThreadService.waitAll();

        // cleanup (TrackSegment use static references for Id, need to remove them !!)
        for (final TrackSegment ts : region_cin.keySet())
            if (ts != null)
                ts.removeId();
        for (final TrackSegment ts : region_cout.keySet())
            if (ts != null)
                ts.removeId();

        // clean other non-necessary stuff right away
        region_cin.clear();
        region_cout.clear();
        allContoursAtTimeT.clear();
        inputData = null;
        edgeData = null;
        regionData = null;
        regionDataSummed = null;
    }

    boolean executeMultiThread(final Collection<Callable<Object>> tasks, final String messageOnError) throws Exception
    {
        try
        {
            // force wait completion for all tasks
            for (final Future<Object> res : multiThreadService.invokeAll(tasks))
                res.get();

            return true;
        }
        catch (final InterruptedException e)
        {
            // restore the interrupted flag
            Thread.currentThread().interrupt();
        }
        catch (final Exception e)
        {
            if (StringUtil.isEmpty(messageOnError))
                throw e;

            e.printStackTrace();
            System.err.println("Warning: " + messageOnError + "(reason: " + e.getMessage() + ").");
        }

        return false;
    }

    private void initData(final int t) throws InterruptedException
    {
        final int edgeChannel = edge_c.getValue().intValue();
        final int regionChannel = region_c.getValue().intValue();

        if (edgeChannel >= inputData.getSizeC())
            throw new IcyHandledException("The selected edge channel is invalid.");
        if (regionChannel >= inputData.getSizeC())
            throw new IcyHandledException("The selected region channel is invalid.");

        final Rectangle3D.Integer bounds = new Rectangle3D.Integer();
        bounds.sizeX = inputData.getSizeX();
        bounds.sizeY = inputData.getSizeY();
        bounds.sizeZ = inputData.getSizeZ();

        // get the current frame (in its original data type)
        final Sequence currentFrame = SequenceUtil.extractFrame(inputData, t);

        // get edge channel bounds (need to load all data first)
        currentFrame.loadAllData();
        final double[] edgeBnds = currentFrame.getChannelBounds(edgeChannel);
        final double[] regionBnds = currentFrame.getChannelBounds(regionChannel);

        // build data scaler to rescaled on [0..1]
        final Scaler edgeScaler = new Scaler(edgeBnds[0], edgeBnds[1], 0d, 1d, false);
        final Scaler regionScaler = new Scaler(regionBnds[0], regionBnds[1], 0d, 1d, false);

        // extract the edge and region data
        edgeData = new Sequence(OMEUtil.createOMEXMLMetadata(inputData.getOMEXMLMetadata()), "edge data");
        regionData = new Sequence(OMEUtil.createOMEXMLMetadata(inputData.getOMEXMLMetadata()), "region data");

        // convert float data rescaled to [0..1]
        for (int z = 0; z < bounds.sizeZ; z++)
        {
            IcyBufferedImage img;

            img = currentFrame.getImage(0, z, edgeChannel);
            img = IcyBufferedImageUtil.convertType(img, DataType.FLOAT, new Scaler[] {edgeScaler});
            edgeData.setImage(0, z, img);

            img = currentFrame.getImage(0, z, regionChannel);
            img = IcyBufferedImageUtil.convertType(img, DataType.FLOAT, new Scaler[] {regionScaler});
            regionData.setImage(0, z, img);
        }

        try
        {
            // smooth the signal
            final Sequence gaussian = Kernels1D.CUSTOM_GAUSSIAN.createGaussianKernel1D(1).toSequence();
            Convolution1D.convolve(edgeData, gaussian, gaussian, null);
            Convolution1D.convolve(regionData, gaussian, gaussian, null);
        }
        catch (final Exception e)
        {
            System.err.println("Warning: error while smoothing the signal: " + e.getMessage());
        }

        // summed region data (use to accelerate intensity calculations)
        regionDataSummed = SequenceUtil.getCopy(regionData);
        for (int z = 0; z < bounds.sizeZ; z++)
        {
            final float[] regionDataSliceSummed = regionDataSummed.getDataXYAsFloat(0, z, 0);

            for (int j = 0; j < bounds.sizeY; j++)
            {
                // start at the second pixel (index 1) of each line
                int offset = (j * bounds.sizeX) + 1;

                // accumulate (add) each pixel with the previous
                for (int i = 1; i < bounds.sizeX; i++, offset++)
                    regionDataSliceSummed[offset] += regionDataSliceSummed[offset - 1];
            }

            // for 'virtual' mode
            regionDataSummed.setDataXY(0, z, 0, regionDataSliceSummed);
        }
    }

    private void initCurrentContours(final int t)
    {
        // prepare parameters
        final int convWinSize = convergence_winSize.getValue().intValue() * 2;
        // use multi threading to initiate current contours (by duplicating ones from
        // previous frame)
        final List<Callable<Object>> tasks = new ArrayList<Callable<Object>>();

        try
        {
            // duplicate contours from the previous frame
            for (final TrackSegment segment : trackGroup.getValue().getTrackSegmentList())
                tasks.add(new ContourDuplicator(segment, t, convWinSize));

            // execute tasks
            executeMultiThread(tasks, "couldn't duplicate a contour");
        }
        catch (final Exception e)
        {
            e.printStackTrace();
            System.err.println("Warning:  (reason: " + e.getMessage() + "). Moving on...");
        }
    }

    private void addNewContours(final int t) throws InterruptedException
    {
        // prepare parameters
        final TrackGroup tg = trackGroup.getValue();
        final List<TrackSegment> tracks = trackGroup.getValue().getTrackSegmentList();
        final int convWinSize = convergence_winSize.getValue().intValue() * 2;
        final Viewer viewer = inputData.getFirstViewer();
        final int currentZ = (viewer != null) ? viewer.getPositionZ() : 0;
        final Tuple3d pixelSize = new Point3d(inputData.getPixelSizeX(), inputData.getPixelSizeY(), inputData.getPixelSizeZ());

        // get active tracks and tracks that ended on previous frame
        final List<TrackSegment> activeTracks = getTracksEndingAt(tracks, t);
        final List<TrackSegment> justEndedTracks = getTracksEndingAt(tracks, t - 1);

        // get all new objects for this frame (ROI format)
        final List<ROI> objects = getNewObjectsFor(t);

        // use multi threading to initiate contours from these new objects
        final List<Callable<Object>> tasks = new ArrayList<Callable<Object>>();
        final List<TrackSegment> newTracks = new ArrayList<TrackSegment>();

        try
        {
            // initialize new contours
            for (final ROI roi : objects)
                tasks.add(new ContourInitializer(roi, currentZ, t, pixelSize, convWinSize, activeTracks, justEndedTracks));

            // force wait completion for all tasks
            for (final Future<Object> res : multiThreadService.invokeAll(tasks))
            {
                final TrackSegment track = (TrackSegment) res.get();

                // a new track was created ? --> add it
                if (track != null)
                    newTracks.add(track);
            }
        }
        catch (final InterruptedException e)
        {
            // restore the interrupted flag
            Thread.currentThread().interrupt();
        }
        catch (final Exception e)
        {
            e.printStackTrace();
            System.err.println("Warning: couldn't initialize a contour (reason: " + e.getMessage() + ").");
        }

        // add new created tracks
        synchronized (tg)
        {
            for (final TrackSegment track : newTracks)
                tg.addTrackSegment(track);
        }
        synchronized (region_cin)
        {
            for (final TrackSegment track : newTracks)
                region_cin.put(track, Double.valueOf(0d));
        }
        synchronized (region_cout)
        {
            for (final TrackSegment track : newTracks)
                region_cout.put(track, Double.valueOf(0d));
        }
    }

    /**
     * Returns all {@link TrackSegment} ending at the specified time point
     */
    private List<TrackSegment> getTracksEndingAt(final List<TrackSegment> tracks, final int t)
    {
        final List<TrackSegment> result = new ArrayList<TrackSegment>();

        if (t >= 0)
        {
            for (final TrackSegment track : tracks)
            {
                // get last detection for this track
                final Detection detection = track.getLastDetection();

                // does it end at specified time point ? --> add track to result list
                if ((detection != null) && (detection.getT() == t))
                    result.add(track);
            }
        }

        return result;
    }

    private boolean hasFutureRois(final int t)
    {
        List<ROI> rois = CollectionUtil.asArrayList(roiInput.getValue());

        // protocol mode, no ROI ? --> no more ROI
        if (rois.isEmpty())
        {
            if (isHeadLess())
                return false;

            // take sequence ROI
            rois = inputData.getROIs();
        }

        // interactive mode, no ROI ? --> no more ROI
        if (rois.isEmpty())
            return false;

        // only pick ROI for current frame
        for (final ROI roi : rois)
        {
            if (roi instanceof ROI2D)
            {
                // we have new ROI for this time point or in future
                if (((ROI2D) roi).getT() >= t)
                    return true;
            }
            else if (roi instanceof ROI3D)
            {
                if (((ROI3D) roi).getT() >= t)
                    return true;
            }
        }

        return false;
    }

    private List<ROI> getRoisOf(final int t)
    {
        List<ROI> result = CollectionUtil.asArrayList(roiInput.getValue());

        // protocol mode, no ROI ? --> cannot continue
        if (result.isEmpty())
        {
            if (isHeadLess())
                throw new VarException(roiInput, "Active contours: no input ROI");

            result = inputData.getROIs();
        }

        // interactive mode, no ROI ? --> cannot continue
        if (result.isEmpty())
            throw new VarException(input.getVariable(), "Please draw or select a ROI");

        // only pick ROI for current frame
        for (int i = result.size() - 1; i >= 0; i--)
        {
            final ROI roi = result.get(i);

            if (roi instanceof ROI2D)
            {
                final int rt = ((ROI2D) roi).getT();

                if (rt == -1)
                {
                    // take T = -1 ROI only on first frame
                    if (t > 0)
                        result.remove(i);
                }
                // take specific T ROI only for the specific frame
                else if (rt != t)
                    result.remove(i);
            }
            else if (roi instanceof ROI3D)
            {
                final int rt = ((ROI3D) roi).getT();

                if (rt == -1)
                {
                    // take T = -1 ROI only on first frame
                    if (t > 0)
                        result.remove(i);
                }
                // take specific T ROI only for the specific frame
                else if (rt != t)
                    result.remove(i);
            }
            else
                // only 2D and 3D ROI are supported
                result.remove(i);
        }

        return result;
    }

    /**
     * Create a copy of input ROI and split multiples components ROI if needed
     * 
     * @throws InterruptedException
     */
    private List<ROI> getWorkRois(final ROI roi) throws InterruptedException
    {
        // get minimum allowed size
        final int minPoints = (int) (contour_resolution.getValue().doubleValue() * 3);
        final List<ROI> result = new ArrayList<ROI>();

        // 2D area ROI (we should also consider 3D area normally)
        if (roi instanceof ROI2DArea)
        {
            final ROI2DArea area = (ROI2DArea) roi;
            // get mask
            final BooleanMask2D mask = area.getBooleanMask(true);
            // get component(s)
            final BooleanMask2D[] components = mask.getComponents();

            // check if the area has multiple components
            if (components.length > 1)
            {
                // split multi components ROI
                for (final BooleanMask2D comp : components)
                {
                    // don't bother initializing contours that are too small (w.r.t. the required
                    // sampling)
                    if (comp.getNumberOfPoints() < minPoints)
                        continue;

                    final ROI2DArea newArea = new ROI2DArea(comp);

                    // set the Z, T and C position
                    newArea.setZ(area.getZ());
                    newArea.setT(area.getT());
                    newArea.setC(area.getC());

                    // add to 'extra ROIS' list
                    result.add(newArea);
                }
            }
            else
            {
                // accept only if above minimal wanted number of point
                if (mask.getNumberOfPoints() >= minPoints)
                    result.add(roi.getCopy());
            }
        }
        else
            // always accept it
            result.add(roi.getCopy());

        return result;
    }

    private List<ROI> getNewObjectsFor(final int t) throws InterruptedException
    {
        final List<ROI> result = new ArrayList<ROI>();

        // get ROI located at the given time point
        for (final ROI roi : getRoisOf(t))
            // separate components and filter if needed
            result.addAll(getWorkRois(roi));

        // watch for entering object (only for 2D)
        if (tracking_newObjects.getValue().booleanValue() && (inputData.getSizeZ() == 1))
        {
            final Collection<Double> allVolumes = volumes.values();
            double minVol = 0d;
            double maxVol = 0d;
            double meanVol = 0d;

            // compute mean volume for object
            if (!allVolumes.isEmpty())
            {
                for (final Double volume : allVolumes)
                {
                    final double v = volume.doubleValue();

                    if ((minVol == 0d) || (v < minVol))
                        minVol = v;
                    if ((maxVol == 0d) || (v > maxVol))
                        maxVol = v;
                    meanVol += v;
                }

                meanVol /= allVolumes.size();
            }

            // adjust min / max allowed volume
            minVol *= 0.3d;
            maxVol *= 4d;
            if (minVol < (meanVol / 5d))
                minVol = meanVol / 5d;
            if (maxVol > (meanVol * 5d))
                maxVol = meanVol * 5d;
            minVol = Math.round(minVol);
            maxVol = Math.round(maxVol);

            // don't try to detect new tiny objects
            if (maxVol > 10)
            {
                // we use HK-Means plugin to detect entering objects in region data only
                // (HK-Mean won't work on edge signal anyway)
                // Note that it will work only for fluo images here
                for (final ROI roi : HKMeans.hKMeans(regionData, (byte) 7, (int) minVol, (int) maxVol, Double.valueOf(0d)))
                {
                    // set correct position for ROI
                    ((ROI2D) roi).setC(-1);
                    ((ROI2D) roi).setZ(-1);
                    ((ROI2D) roi).setT(t);

                    result.add(roi);
                }
            }
        }

        return result;
    }

    /**
     * Return new contour from a given ROI.
     * 
     * @throws InterruptedException
     */
    ActiveContour getContourOf(final ROI roi, final int z, final int t, final Tuple3d pixelSize, final int convWinSize) throws InterruptedException
    {
        final int depth = inputData.getSizeZ();
        ActiveContour result = null;

        try
        {
            // fix Z position for 2D ROI
            if (roi instanceof ROI2D)
            {
                final ROI2D roi2d = (ROI2D) roi;

                // infinite Z dim ?
                if (roi2d.getZ() == -1)
                {
                    // a 2D contour cannot be created from a "virtually 3D" ROI, merely a slice of
                    // it => which one?
                    if (z == -1)
                        throw new EzException(ActiveContours.this, "Please select a 2D slice (using a 2D viewer) where the contour should operate", true);

                    if (depth > 1)
                        System.err.println("WARNING: ROI " + roi.getName() + "has a infinite Z dimension, 2D contour will use Z=" + z);

                    roi2d.setZ(z);
                }

                result = new Polygon2D(contour_resolution.getVariable(), new SlidingWindow(convWinSize), roi2d);
                result.setDivisionSensitivity(division_sensitivity.getVariable());
                result.setT(t);

            }
            else if (roi instanceof ROI3D)
            {
                result = new Mesh3D(contour_resolution.getVariable(), (Tuple3d) pixelSize.clone(), (ROI3D) roi, new SlidingWindow(convWinSize));
                // contour.setX(r3.getBounds3D().getCenterX());
                // contour.setY(r3.getBounds3D().getCenterY());
                result.setT(t);
            }
            else
                System.err.println("Warning: couldn't create a contour for ROI '" + roi.getName() + "' (ROI not supported)");
        }
        catch (final TopologyException topo)
        {
            System.err.println("Warning: couldn't create a contour for ROI '" + roi.getName() + "'");
        }

        return result;
    }

    // /**
    // * Return new contour in TrackSegment format from a given ROI.<br>
    // * (we can have several contours if ROI has multiple 'components')
    // */
    // List<TrackSegment> getContoursOf(ROI r, int z, int t, Tuple3d pixelSize, int
    // convWinSize)
    // {
    // final List<TrackSegment> result = new ArrayList<TrackSegment>();
    //
    // for (ROI roi : getWorkRois(r))
    // {
    // final ActiveContour contour = getContourOf(roi, z, t, pixelSize,
    // convWinSize);
    //
    // if (contour != null)
    // {
    // final TrackSegment ts = new TrackSegment();
    // ts.addDetection(contour);
    // result.add(ts);
    // }
    // }
    //
    // return result;
    // }
    //
    // private void addNewContoursOf(int t)
    // {
    // final Viewer viewer = inputData.getFirstViewer();
    // final int currentZ = (viewer != null) ? viewer.getPositionZ() : 0;
    // final Tuple3d pixelSize = new Point3d(inputData.getPixelSizeX(),
    // inputData.getPixelSizeY(),
    // inputData.getPixelSizeZ());
    // final List<ROI> rois = getRoisOf(t);
    // final List<Callable<Object>> tasks = new
    // ArrayList<Callable<Object>>(rois.size());
    //
    // try
    // {
    // // create tasks
    // for (ROI roi : rois)
    // tasks.add(new ContourInitializer(roi, currentZ, t, pixelSize));
    //
    // // execute tasks
    // executeMultiThread(tasks, "couldn't initialise a contour");
    // }
    // catch (Exception e)
    // {
    // e.printStackTrace();
    // System.err.println("Warning: (reason: " + e.getMessage() + "). Moving
    // on...");
    // }
    // }

    public int evolveContours(final int t, final ROI boundField) throws InterruptedException
    {
        // retrieve the contours on the current frame and store them in currentContours
        allContoursAtTimeT.clear();

        for (final TrackSegment segment : trackGroup.getValue().getTrackSegmentList())
        {
            final Detection det = segment.getDetectionAtTime(t);
            if (det != null)
                allContoursAtTimeT.add((ActiveContour) det);
        }

        // nothing to do
        if (allContoursAtTimeT.size() == 0)
            return -1;

        int iter = 0;
        int nbConvergedContours = 0;
        boolean hasContour3d = false;
        final EzGUI ui = getUI();  

        while (!globalStop && (nbConvergedContours < allContoursAtTimeT.size()))
        {
            nbConvergedContours = 0;

            // prepare the list of current evolving (i.e. non-converged) contours
            evolvingContoursAtTimeT.clear();
            for (final ActiveContour contour : allContoursAtTimeT)
            {
                if ((contour.getLastConvergedFrame() == t) || contour.hasConverged(convergence_operation.getValue(), convergence_criterion.getValue()))
                {
                    contour.setLastConvergedFrame(t);
                    nbConvergedContours++;
                    continue;
                }

                // need to know if we have 3D contour..
                if (contour instanceof Mesh3D)
                    hasContour3d = true;

                // if the contour hasn't converged yet, store it for the main loop
                evolvingContoursAtTimeT.add(contour);
            }

            // nothing to do..
            if (evolvingContoursAtTimeT.size() == 0)
                break;

            if (ui != null)
            {
                if (nbConvergedContours == 0)
                    ui.setProgressBarValue(Double.NaN);
                else
                    ui.setProgressBarValue((double) nbConvergedContours / allContoursAtTimeT.size());

                // getUI().setProgressBarMessage("" + iter); // slows down the AWT !!
            }

            // re-sample the contours to ensure homogeneous resolution
            resampleContours(t);

            // we are based on region ?
            if (region_weight.getValue() > EPSILON)
            {
                // update region information (if necessary):
                // - every 10 iterations
                // if the contour list has changed
                boolean updateRegionStatistics = iter % (convergence_winSize.getValue() / 3) == 0;

                for (final ActiveContour contour : allContoursAtTimeT)
                {
                    // make sure this contour's statistics exist
                    if (region_cout.containsKey(trackGroup.getValue().getTrackSegmentWithDetection(contour)))
                        continue;

                    updateRegionStatistics = true;
                    break;
                }

                if (updateRegionStatistics)
                    updateRegionStatistics();
            }

            // compute deformations issued from the energy minimization
            deformContours(boundField);

            // compute energy
            // computeEnergy(mainService, allContours);

            // if we have 3D contour, force VTK garbage collection from time to time (ugly
            // method but we always end with unreleased objects with VTK)
            final long time = System.currentTimeMillis();
            if (hasContour3d && ((iter & 0x1FF) == 0) || ((time - lastVtkGCTime) > 10000L))
            {
                vtkObjectBase.JAVA_OBJECT_MANAGER.gc(false);
                lastVtkGCTime = time;
            }

            if (!Icy.getMainInterface().isHeadLess())
                overlay.painterChanged();

            // maximum number of iteration allowed raised ?
            if (iter >= convergence_nbIter.getValue())
            {
                // consider that all current evolving contours converged here
                for (final ActiveContour contour : evolvingContoursAtTimeT)
                    contour.setLastConvergedFrame(t);

                // stop here
                break;
            }

            iter++;

            if (Thread.currentThread().isInterrupted())
                globalStop = true;
        }

        return iter;
    }

    /**
     * Deform contours together
     * 
     * @param evolvingContours
     * @param allContours
     * @throws InterruptedException
     * @deprecated parameters evolvingContours and allContours are not necessary
     *             anymore (they are now instance variables)
     */
    @Deprecated
    public void deformContours(final HashSet<ActiveContour> evolvingContours, final HashSet<ActiveContour> allContours, final ROI field)
            throws InterruptedException
    {
        deformContours(field);
    }

    /**
     * Deform contours together
     * 
     * @throws InterruptedException
     */
    public void deformContours(final ROI boundField) throws InterruptedException
    {
        if (evolvingContoursAtTimeT.size() == 1 && allContoursAtTimeT.size() == 1)
        {
            // no multi-threading needed

            final ActiveContour contour = evolvingContoursAtTimeT.iterator().next();
            final TrackSegment segment = trackGroup.getValue().getTrackSegmentWithDetection(contour);

            if (Math.abs(edge_weight.getValue()) > EPSILON)
            {
                contour.computeEdgeForces(edgeData, 0, edge_weight.getValue());
            }

            if (regul_weight.getValue() > EPSILON)
            {
                contour.computeInternalForces(regul_weight.getValue());
            }

            if (region_weight.getValue() > EPSILON)
            {
                final double cin = region_cin.get(segment);
                final double cout = region_cout.get(segment);
                contour.computeRegionForces(regionData, 0, region_weight.getValue(), region_sensitivity.getValue(), cin, cout);
            }

            if (axis_weight.getValue() > EPSILON)
            {
                contour.computeAxisForces(axis_weight.getValue());
            }

            if (Math.abs(balloon_weight.getValue()) > EPSILON)
            {
                contour.computeBalloonForces(balloon_weight.getValue());
            }

            if (volume_constraint.getValue() && volumes.containsKey(segment))
            {
                contour.computeVolumeConstraint(volumes.get(segment), volume_weight.getValue());
            }

            contour.move(boundField, contour_timeStep.getValue());
        }
        else
        {
            final ArrayList<Callable<Object>> tasks = new ArrayList<Callable<Object>>(evolvingContoursAtTimeT.size());

            for (final ActiveContour contour : evolvingContoursAtTimeT)
            {
                final TrackSegment segment = trackGroup.getValue().getTrackSegmentWithDetection(contour);

                if (!region_cin.containsKey(segment) && region_weight.getValue() > EPSILON)
                    updateRegionStatistics();

                tasks.add(new Callable<Object>()
                {
                    @Override
                    public Object call()
                    {

                        if (regul_weight.getValue() > EPSILON)
                        {
                            contour.computeInternalForces(regul_weight.getValue());
                        }

                        if (Math.abs(edge_weight.getValue()) > EPSILON)
                        {
                            contour.computeEdgeForces(edgeData, 0, edge_weight.getValue());
                        }

                        if (region_weight.getValue() > EPSILON)
                        {
                            final double cin = region_cin.get(segment);
                            final double cout = region_cout.get(segment);
                            contour.computeRegionForces(regionData, 0, region_weight.getValue(), region_sensitivity.getValue(), cin, cout);
                        }

                        if (axis_weight.getValue() > EPSILON)
                        {
                            contour.computeAxisForces(axis_weight.getValue());
                        }

                        if (Math.abs(balloon_weight.getValue()) > EPSILON)
                        {
                            contour.computeBalloonForces(balloon_weight.getValue());
                        }

                        if (coupling_flag.getValue())
                        {
                            // Don't move the contours just now: coupling feedback must be computed
                            // against ALL contours (including those which have already converged)
                            for (final ActiveContour otherContour : allContoursAtTimeT)
                            {
                                if (otherContour == null || otherContour == contour)
                                    continue;

                                contour.computeFeedbackForces(otherContour);
                            }

                            // we disabled volume constraint feature as it doesn't work and sometime end to
                            // crazy contour (Stephane)
                            // re-enabled volume constraint after taking into account both shrinking and
                            // growing contours instead of just growing ones(Daniel)
                            if (volume_constraint.getValue() && volumes.containsKey(segment))
                            {
                                contour.computeVolumeConstraint(volumes.get(segment), volume_weight.getValue());
                            }
                        }
                        else
                        {
                            // move contours asynchronously
                            contour.move(boundField, contour_timeStep.getValue());
                        }

                        return contour;
                    }
                });
            }

            try
            {
                executeMultiThread(tasks, null);
            }
            catch (final Exception e)
            {
                throw new RuntimeException(e);
            }

            if (coupling_flag.getValue())
            {
                // motion is synchronous, and can be done now
                for (final ActiveContour contour : evolvingContoursAtTimeT)
                    contour.move(boundField, contour_timeStep.getValue());
            }
        }
    }

    /**
     * Resample all contours to maintain a homogeneous resoltution
     * 
     * @param evolvingContours
     * @param allContours
     * @param t
     * @return <code>true</code> if the list of contours has changed (a contour has
     *         vanished or divided), <code>false</code> otherwise
     * @throws InterruptedException
     */
    private void resampleContours(final int t) throws InterruptedException
    {
        // if (isHeadLess()) System.out.println("=> Resampling contours...");

        final VarBoolean loop = new VarBoolean("loop", true);

        final VarBoolean change = new VarBoolean("change", false);

        final int maxIterations = 10000;

        int itCount = 0;
        while (loop.getValue())
        {
            if (itCount++ > maxIterations || Thread.currentThread().isInterrupted())
                break;

            loop.setValue(false);

            if (evolvingContoursAtTimeT.size() == 1)
            {
                // no multi-threading needed
                for (final ActiveContour contour : evolvingContoursAtTimeT)
                {
                    if (new ReSampler(trackGroup.getValue(), contour, evolvingContoursAtTimeT, allContoursAtTimeT).call().booleanValue())
                    {
                        change.setValue(true);
                        loop.setValue(true);
                    }
                }
            }
            else
            {
                final List<ReSampler> tasks = new ArrayList<ReSampler>(evolvingContoursAtTimeT.size());

                for (final ActiveContour contour : evolvingContoursAtTimeT)
                    tasks.add(new ReSampler(trackGroup.getValue(), contour, evolvingContoursAtTimeT, allContoursAtTimeT));

                try
                {
                    for (final Future<Boolean> resampled : multiThreadService.invokeAll(tasks))
                    {
                        if (resampled.get())
                        {
                            change.setValue(true);
                            loop.setValue(true);
                        }
                    }
                }
                catch (final InterruptedException e)
                {
                    Thread.currentThread().interrupt();
                }
                catch (final ExecutionException e)
                {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                catch (final RuntimeException e)
                {
                    throw e;
                }
            }
        }

        if (change.getValue() && region_weight.getValue() > EPSILON)
            updateRegionStatistics();
    }

    private void updateRegionStatistics() throws InterruptedException
    {
        updateRegionStatistics(region_localise.getValue());
    }

    private void updateRegionStatistics(final boolean locally) throws InterruptedException
    {
        final int nbContours = allContoursAtTimeT.size();

        if (nbContours == 0)
            return;

        // use a global mask for global statistics
        if (!locally)
            for (final BooleanMask2D slice : contourMask_buffer.mask.values())
                Arrays.fill(slice.mask, false);

        if (nbContours == 1)
        {
            // use the current thread
            for (final ActiveContour contour : allContoursAtTimeT)
            {
                new LocalRegionStatisticsComputer(contour, !locally).call();
            }
        }
        else
        {
            // use multiple threads
            final Collection<Callable<Object>> updaters = new ArrayList<Callable<Object>>(allContoursAtTimeT.size());
            for (final ActiveContour contour : allContoursAtTimeT)
                updaters.add(new LocalRegionStatisticsComputer(contour, !locally));

            try
            {
                executeMultiThread(updaters, null);
            }
            catch (final Exception e)
            {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }

        updateBackgroundStatistics(locally);
    }

    private void updateBackgroundStatistics(final boolean locally)
    {
        final int nbContours = allContoursAtTimeT.size();

        if (nbContours == 0)
            return;

        if (locally)
        {
            if (nbContours == 1)
            {
                // use the current thread
                new LocalBackgroundStatisticsComputer(allContoursAtTimeT.iterator().next()).call();
            }
            else
            {
                // use multiple threads
                final Collection<Callable<Object>> updaters = new ArrayList<Callable<Object>>(allContoursAtTimeT.size());
                for (final ActiveContour contour : allContoursAtTimeT)
                    updaters.add(new LocalBackgroundStatisticsComputer(contour));

                try
                {
                    executeMultiThread(updaters, null);
                }
                catch (final Exception e)
                {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }
        else
        {
            final double[] outs = new double[inputData.getSizeZ()];

            for (int z = 0; z < outs.length; z++)
            {
                final boolean[] _mask = contourMask_buffer.mask.get(z).mask;
                final float[] _data = regionData.getDataXYAsFloat(0, z, 0);
                double outSumSlice = 0, outCptSlice = 0;

                for (int i = 0; i < _mask.length; i++)
                {
                    if (!_mask[i])
                    {
                        outSumSlice += _data[i];
                        outCptSlice++;
                    }
                }

                outs[z] = (outCptSlice == 0d) ? 0d : outSumSlice / outCptSlice;
            }

            for (final ActiveContour contour : allContoursAtTimeT)
            {
                final TrackSegment segment = trackGroup.getValue().getTrackSegmentWithDetection(contour);
                if (contour instanceof Polygon2D)
                {
                    final double cout = outs[(int) Math.round(contour.getZ())];
                    region_cout.put(segment, cout);
                    // System.out.println(" out: " + cout);
                }
                else
                {
                    final double cout = ArrayMath.mean(outs);
                    region_cout.put(segment, cout);
                    // System.out.println(" out: " + cout);
                }
            }
        }
    }

    private void storeResult(final int t) throws UnsupportedOperationException, InterruptedException
    {
        if (isHeadLess())
            System.out.println("=> Storing result...");

        final List<TrackSegment> segments = trackGroup.getValue().getTrackSegmentList();
        // Append the current list to the existing one
        final List<ROI> rois = new ArrayList<ROI>(Arrays.asList(roiOutput.getValue()));

        for (int i = 1; i <= segments.size(); i++)
        {
            final TrackSegment segment = segments.get(i - 1);

            final ActiveContour contour = (ActiveContour) segment.getDetectionAtTime(t);
            if (contour == null)
                continue;

            // Update volume information
            if (!volumes.containsKey(segment))
                volumes.put(segment, contour.getDimension(2));

            // output as ROIs
            final ROI roi = contour.toROI(output_roiType.getValue(), inputData);

            if (roi != null)
            {
                // store id into ROI property
                roi.setProperty(CONTOUR_ID, Integer.toString(i));
                // use contour name and color
                roi.setName(contour.getName());
                roi.setColor(contour.getColor());

                if (roi instanceof ROI2D)
                    ((ROI2D) roi).setT(t);
                else if (roi instanceof ROI3D)
                    ((ROI3D) roi).setT(t);

                rois.add(roi);
            }

            // output labels
            if (output_labels.isReferenced() || output_rois.getValue() == ExportROI.AS_LABELS)
            {
                Sequence binSeq = output_labels.getValue();
                if (binSeq == null)
                {
                    output_labels.setValue(binSeq = new Sequence());
                }
                if (binSeq.getImage(t, (int) contour.getZ()) == null)
                {
                    binSeq.setImage(t, (int) contour.getZ(), new IcyBufferedImage(inputData.getWidth(), inputData.getHeight(), 1, DataType.USHORT));
                }
                contour.toSequence(binSeq, i);
            }
        }

        if (output_labels.getValue() != null)
            output_labels.getValue().dataChanged();

        if (rois.size() > 0)
            roiOutput.setValue(rois.toArray(new ROI[rois.size()]));

        // stop everything if there are no more contours to evolve
        // if (noResultsOnCurrentFrame && !tracking_newObjects.getValue())
        // globalStop = true;
    }

    @Override
    public void clean()
    {
        if (inputData != null)
            inputData.removeOverlay(overlay);
        if (trackGroup.getValue() != null)
            trackGroup.getValue().clearAllTrackSegment();

        // end execution
        multiThreadService.shutdownNow();
        multiThreadService.waitAll();

        // cleanup (TrackSegment use static references for Id, need to remove them !!)
        for (final TrackSegment ts : region_cin.keySet())
            if (ts != null)
                ts.removeId();
        for (final TrackSegment ts : region_cout.keySet())
            if (ts != null)
                ts.removeId();

        region_cin.clear();
        region_cout.clear();
        volumes.clear();
        evolvingContoursAtTimeT.clear();
        allContoursAtTimeT.clear();
        inputData = null;
        edgeData = null;
        regionData = null;
        regionDataSummed = null;
    }

    @Override
    public void declareInput(final VarList inputMap)
    {
        inputMap.add("input sequence", input.getVariable());
        inputMap.add("Input ROI", roiInput);
        inputMap.add("regularization: weight", regul_weight.getVariable());
        inputMap.add("edge: weight", edge_weight.getVariable());
        edge_c.setActive(false);
        edge_c.setValues(0, 0, 16, 1);
        inputMap.add("edge: channel", edge_c.getVariable());
        inputMap.add("region: weight", region_weight.getVariable());
        inputMap.add("region: sensitivity", region_sensitivity.getVariable());
        region_c.setActive(false);
        region_c.setValues(0, 0, 16, 1);
        inputMap.add("region: channel", region_c.getVariable());

        inputMap.add("balloon: weight", balloon_weight.getVariable());

        coupling_flag.setValue(true);
        inputMap.add("contour resolution", contour_resolution.getVariable());
        contour_resolution.addVarChangeListener(new EzVarListener<Double>()
        {
            @Override
            public void variableChanged(final EzVar<Double> source, final Double newValue)
            {
                convergence_winSize.setValue((int) (100.0 / newValue));
            }
        });

        // inputMap.add("minimum object size", contour_minArea.getVariable());
        inputMap.add("region bounds", evolution_bounds.getVariable());
        evolution_bounds.getVariable().setValue(null);
        inputMap.add("time step", contour_timeStep.getVariable());
        // inputMap.add("convergence window size", convergence_winSize.getVariable());
        inputMap.add("convergence value", convergence_criterion.getVariable());
        inputMap.add("max. iterations", convergence_nbIter.getVariable());
        inputMap.add("type of ROI output", output_roiType.getVariable());
        inputMap.add("tracking", tracking.getVariable());
        inputMap.add("division sensitivity", division_sensitivity.getVariable());
        inputMap.add("axis constraint", axis_weight.getVariable());
        volume_constraint.setValue(true);
        inputMap.add("Volume weight", volume_weight.getVariable());
        inputMap.add("watch entering objects", tracking_newObjects.getVariable());
    }

    @Override
    public void declareOutput(final VarList outputMap)
    {
        outputMap.add("Regions of interest", roiOutput);
        outputMap.add("Tracks", trackGroup);
        outputMap.add("Labels", output_labels);
    }
}
