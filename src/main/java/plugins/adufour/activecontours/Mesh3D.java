package plugins.adufour.activecontours;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

import org.w3c.dom.Node;

import com.jogamp.graph.geom.Triangle;

import icy.canvas.IcyCanvas;
import icy.painter.Overlay;
import icy.roi.BooleanMask2D;
import icy.roi.BooleanMask3D;
import icy.roi.ROI;
import icy.roi.ROI3D;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.rectangle.Rectangle3D;
import plugins.adufour.activecontours.ActiveContours.ROIType;
import plugins.adufour.activecontours.SlidingWindow.Operation;
import plugins.adufour.roi.mesh.MeshTopologyException;
import plugins.adufour.roi.mesh.Vertex3D;
import plugins.adufour.roi.mesh.polygon.ROI3DTriangularMesh;
import plugins.adufour.vars.lang.Var;
import plugins.kernel.roi.descriptor.intensity.ROIMeanIntensityDescriptor;
import plugins.kernel.roi.roi3d.ROI3DArea;

/**
 * Mesh3D is the behind structure for 3D representation of Active Contour.<br>
 * While it uses internally the <code>ActiveMesh</code> which is inherited from {@link ROI3DTriangularMesh}, Mesh 3D has to work using the pixel size for
 * accurate active contour computation so pixel size is handled on Mesh3D side and not directly in the ActiveMesh ROI which should be expressed in <i>voxel</i>
 * as any ROI.
 */
public class Mesh3D extends ActiveContour
{
    /**
     * an active vertex is a vertex that carries motion information
     * 
     * @author Alexandre Dufour
     */
    private static class ActiveVertex extends Vertex3D
    {
        public final Vector3d imageForces = new Vector3d();
        public final Vector3d internalForces = new Vector3d();
        public final Vector3d feedbackForces = new Vector3d();
        public final Vector3d volumeConstraint = new Vector3d();

        public ActiveVertex(ActiveVertex v)
        {
            super(v.position, v.neighbors);
        }

        public ActiveVertex(Point3d position)
        {
            this(position, 0);
        }

        public ActiveVertex(Point3d position, int nbNeighbors)
        {
            super(position, nbNeighbors);
        }

        @Override
        public Vertex3D clone()
        {
            return new ActiveVertex(this);
        }
    }

    /**
     * An ActiveMesh is defined here as a 3D surface mesh with {@link Triangle} as elementary face
     * and {@link ActiveVertex} as elementary vertex
     * 
     * @author Alexandre Dufour
     */
    public static class ActiveMesh extends ROI3DTriangularMesh
    {
        public ActiveMesh()
        {

        }

        public ActiveMesh(double sampling, ROI3D roi)
        {
            super(sampling, roi);

            // setName("Contour (" + roi.getName() + ")");
            // better to just keep original name, AC will rename it later
            setName(roi.getName());
        }

        @Override
        public ActiveVertex createVertex(Point3d position)
        {
            return new ActiveVertex(position);
        }
    }

    // internal 3D ActiveMesh ROI: should be expressed in voxel !
    // Mesh3D does the pixel size conversion as needed
    final ActiveMesh mesh;
    final Tuple3d pixelSize;

    final Map<IcyCanvas, Overlay> overlays;
    boolean isRemoving;

    /**
     * DO NOT USE! This constructor is for XML loading purposes only
     */
    public Mesh3D()
    {
        super();

        mesh = new ActiveMesh();
        pixelSize = new Point3d();

        overlays = new HashMap<IcyCanvas, Overlay>();
        isRemoving = false;
    }

    /**
     * Creates a clone of the specified contour
     * 
     * @param contour
     */
    public Mesh3D(Mesh3D contour)
    {
        super(contour.sampling, new SlidingWindow(contour.convergence.getSize()));

        mesh = (ActiveMesh) contour.mesh.clone();
        pixelSize = new Point3d();
        setName(contour.getName());

        overlays = new HashMap<IcyCanvas, Overlay>();
        isRemoving = false;

        setColor(contour.getColor());

        updateMetaData();
    }

    public Mesh3D(Var<Double> sampling, Tuple3d pixelSize, ROI3D roi, SlidingWindow convergenceWindow)
    {
        super(sampling, convergenceWindow);

        mesh = new ActiveMesh(sampling.getValue(), roi);
        mesh.setColor(getColor());
        setName(roi.getName());

        this.pixelSize = pixelSize;

        overlays = new HashMap<IcyCanvas, Overlay>();
        isRemoving = false;

        updateMetaData();
    }

    /**
     * Update the axis constraint force, which adjusts the takes the final forces and normalize them
     * to keep the contour shape along its principal axis <br>
     * WARNING: this method directly update the array of final forces used to displace the contour
     * points. It should be used among the last to keep it most effective
     * 
     * @param weight
     */
    @Override
    void computeAxisForces(double weight)
    {
        final Vector3d axis = mesh.getMajorAxis(null);

        // To drive the contour along the main object axis, each displacement
        // vector is scaled by the scalar product between its normal and the main axis.

        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            final ActiveVertex av = ((ActiveVertex) v);

            // dot product between normalized vectors ranges from -1 to 1
            final double colinearity = Math.abs(v.normal.dot(axis)); // now from 0 to 1
            // goal: adjust the minimum using the weight, but keep max to 1
            final double threshold = Math.max(colinearity, 1 - weight);

            av.imageForces.scale(threshold);

            // if (Double.isNaN(av.imageForces.x) || Double.isNaN(av.imageForces.y) || Double.isNaN(av.imageForces.z))
            // {
            // System.out.println("oups !");
            // }
        }
    }

    @Override
    void computeBalloonForces(double weight)
    {
        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            final ActiveVertex av = ((ActiveVertex) v);

            av.imageForces.scaleAdd(weight, v.normal, ((ActiveVertex) v).imageForces);

            // if (Double.isNaN(av.imageForces.x) || Double.isNaN(av.imageForces.y) || Double.isNaN(av.imageForces.z))
            // {
            // System.out.println("oups !");
            // }
        }
    }

    /**
     * Update edge term of the contour evolution according to the image gradient
     * 
     * @param weight
     * @param edgeData
     */
    @Override
    void computeEdgeForces(Sequence edgeData, int channel, double weight)
    {
        final Vector3d grad = new Vector3d();
        final Point3d prev = new Point3d();
        final Point3d p = new Point3d();

        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            final ActiveVertex av = ((ActiveVertex) v);

            p.set(v.position);

            // compute the gradient (2nd order)
            grad.x = getPixelValue(edgeData, p.x + 0.5, p.y, p.z);
            grad.y = getPixelValue(edgeData, p.x, p.y + 0.5, p.z);
            grad.z = getPixelValue(edgeData, p.x, p.y, p.z + 0.5);

            prev.x = getPixelValue(edgeData, p.x - 0.5, p.y, p.z);
            prev.y = getPixelValue(edgeData, p.x, p.y - 0.5, p.z);
            prev.z = getPixelValue(edgeData, p.x, p.y, p.z - 0.5);

            grad.sub(prev);
            grad.scale(weight);

            av.imageForces.add(grad);

            // if (Double.isNaN(av.imageForces.x) || Double.isNaN(av.imageForces.y) || Double.isNaN(av.imageForces.z))
            // {
            // System.out.println("oups !");
            // }
        }
    }

    @Override
    void computeRegionForces(Sequence imageData, int channel, double weight, double sensitivity, double cin,
            double cout)
    {
        // sensitivity should be high for dim objects, low for bright objects...
        // ... but none of the following options work properly
        // sensitivity *= 1/(1+cin);
        // sensitivity = sensitivity * cin / (0.01 + cout);
        // sensitivity = sensitivity / (2 * Math.max(cout, cin));
        // sensitivity = sensitivity / (Math.log10(cin / cout));

        final Vector3d regionForce = new Vector3d();
        final double adjWeight = weight * sampling.getValue();

        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            final ActiveVertex av = ((ActiveVertex) v);

            final Point3d p = v.position;
            final double val = getPixelValue(imageData, p.x, p.y, p.z);
            double inDiff, outDiff;

            inDiff = val - cin;
            inDiff *= inDiff;

            outDiff = val - cout;
            outDiff *= outDiff;

            regionForce.set(v.normal);
            regionForce.scale(adjWeight * (sensitivity * outDiff) - (inDiff / sensitivity));

            av.imageForces.add(regionForce);

            // if (Double.isNaN(av.imageForces.x) || Double.isNaN(av.imageForces.y) || Double.isNaN(av.imageForces.z))
            // {
            // System.out.println("oups !");
            // }
        }
    }

    @Override
    void computeInternalForces(double weight)
    {
        final Vector3d internalForce = new Vector3d();
        final double adjWeight = weight / sampling.getValue();

        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            final ActiveVertex av = (ActiveVertex) v;

            internalForce.scale(-v.neighbors.size(), v.position);

            for (Integer nn : v.neighbors)
                internalForce.add(mesh.getVertex(nn).position);

            internalForce.scale(adjWeight);

            av.internalForces.add(internalForce);

            // if (Double.isNaN(av.internalForces.x) || Double.isNaN(av.internalForces.y) || Double.isNaN(av.internalForces.z))
            // {
            // System.out.println("oups !");
            // }
        }
    }

    // TODO take into account weight
    void computeVolumeConstraint(double targetVolume, double weight)
    {
        // 1) compute the difference between target and current volume
        final double volumeDiff = targetVolume - getDimension(2);
        // if (volumeDiff > 0): contour too small, should no longer shrink
        // if (volumeDiff < 0): contour too big, should no longer grow

        final Vector3d avgFeedback = new Vector3d();
        int cpt = 0;

        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            final ActiveVertex av = ((ActiveVertex) v);

            final Vector3d feedbackForce = av.feedbackForces;
            // 2) check whether the final force has same direction as the outer normal
            final double forceNorm = av.imageForces.dot(v.normal);

            // if (Double.isNaN(av.imageForces.x) || Double.isNaN(av.imageForces.y) || Double.isNaN(av.imageForces.z))
            // {
            // System.out.println("oups !");
            // }

            // if forces have same direction (forceNorm > 0): contour is growing
            // if forces have opposite direction (forceNorm < 0): contour is shrinking

            // estimate an average feedback
            if ((forceNorm > 0) && (volumeDiff < 0))
            {
                avgFeedback.add(feedbackForce);
                cpt++;
            }
        }

        if (avgFeedback.length() > 0)
        {
            avgFeedback.scale(1d / cpt);
            avgFeedback.scale(Math.abs(volumeDiff / targetVolume) / 1.5);

            // if (Double.isNaN(avgFeedback.x) || Double.isNaN(avgFeedback.y) || Double.isNaN(avgFeedback.z))
            // {
            // System.out.println("oups !");
            // }

            // move the entire mesh (ugly, but amazingly efficient!!)
            for (Vertex3D v : mesh.getVertices())
            {
                if (v != null)
                    ((ActiveVertex) v).volumeConstraint.add(avgFeedback);
            }
        }
    }

    /**
     * Computes the feedback forces yielded by the penetration of the current contour into the
     * target contour
     * 
     * @param target
     *        the contour that is being penetrated
     * @return the number of actual point-mesh intersection tests
     */
    @Override
    int computeFeedbackForces(ActiveContour target)
    {
        final Vector3d feedbackForce = new Vector3d();
        int tests = 0;

        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            if (!target.boundingBox.intersect(v.position))
                continue;
            if (!target.boundingSphere.intersect(v.position))
                continue;

            final ActiveVertex av = (ActiveVertex) v;

            tests++;

            final double feedback = target.getDistanceToEdge(v.position);
            if (feedback > 0)
            {
                feedbackForce.set(v.normal);
                feedbackForce.scale(-feedback * 10);
                av.feedbackForces.add(feedbackForce);

                // if (Double.isNaN(av.feedbackForces.x) || Double.isNaN(av.feedbackForces.y) || Double.isNaN(av.feedbackForces.z))
                // {
                // System.out.println("oups !");
                // }
            }
        }

        return tests;
    }

    public double getCurvature(Point3d pt)
    {
        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            if (!v.position.equals(pt))
                continue;

            final Vector3d sum = new Vector3d();
            final Vector3d diff = new Vector3d();
            for (Integer n : v.neighbors)
            {
                final Point3d neighbor = mesh.getVertex(n).position;

                diff.sub(neighbor, pt);
                sum.add(diff);
            }
            sum.scale(1d / v.neighbors.size());

            return sum.length() * Math.signum(sum.dot(v.normal));
        }

        return 0d;
    }

    public double getDimension(int order)
    {
        switch (order)
        {
            case 0:
                return mesh.getNumberOfVertices(true);
            case 1:
                return mesh.getNumberOfContourPoints();
            case 2:
                return mesh.getNumberOfPoints();
        }
        return Double.NaN;
    }

    // Point3d getMassCenter(boolean convertToImageSpace)
    // {
    // return mesh.getMassCenter(convertToImageSpace);
    // }

    Point3d getMassCenter()
    {
        // here we always want in metrics unit
        return mesh.getMassCenter(pixelSize);
    }

    /**
     * @return The major axis of this contour, i.e. an unnormalized vector formed of the two most
     *         distant contour points
     */
    public Vector3d getMajorAxis()
    {
        // here we always want in metrics unit
        return mesh.getMajorAxis(pixelSize);
    }

    /**
     * Calculates the 3D image value at the given coordinates (in voxel units) by tri-linear
     * interpolation
     * 
     * @param imageFloat
     *        the image to sample (must be of type {@link DataType#FLOAT})
     * @param x
     *        the X-coordinate of the point
     * @param y
     *        the Y-coordinate of the point
     * @param z
     *        the Z-coordinate of the point
     * @return the interpolated image value at the given coordinates
     */
    private static double getPixelValue(Sequence data, double x, double y, double z)
    {
        return data.getDataInterpolated(0, z, 0, y, x);

        // // "center" the coordinates to the center of the pixel
        // x -= 0.5;
        // y -= 0.5;
        // z -= 0.5;
        //
        // int width = data.getSizeX();
        // int height = data.getSizeY();
        // int depth = data.getSizeZ();
        //
        // final int i = (int) Math.floor(x);
        // final int j = (int) Math.floor(y);
        // final int k = (int) Math.floor(z);
        //
        // if (i < 0 || i >= width - 1)
        // return 0;
        // if (j < 0 || j >= height - 1)
        // return 0;
        // if (k < 0 || k >= depth - 1)
        // return 0;
        //
        // final int pixel = i + j * width;
        // final int east = pixel + 1; // saves 3 additions
        // final int south = pixel + width; // saves 1 addition
        // final int southeast = south + 1; // saves 1 addition
        //
        // float[] currSlice = data.getDataXYAsFloat(0, k, 0);
        // float[] nextSlice = data.getDataXYAsFloat(0, k + 1, 0);
        //
        // float value = 0;
        //
        // x -= i;
        // y -= j;
        // z -= k;
        //
        // final double mx = 1 - x;
        // final double my = 1 - y;
        // final double mz = 1 - z;
        //
        // value += mx * my * mz * currSlice[pixel];
        // value += x * my * mz * currSlice[east];
        // value += mx * y * mz * currSlice[south];
        // value += x * y * mz * currSlice[southeast];
        // value += mx * my * z * nextSlice[pixel];
        // value += x * my * z * nextSlice[east];
        // value += mx * y * z * nextSlice[south];
        // value += x * y * z * nextSlice[southeast];
        //
        // return value;
    }

    @Override
    public double getX()
    {
        // get this in pixel units
        return mesh.getMassCenter(null).x;
    }

    @Override
    public double getY()
    {
        // get this in pixel units
        return mesh.getMassCenter(null).y;
    }

    @Override
    public double getZ()
    {
        // get this in pixel units
        return mesh.getMassCenter(null).z;
    }

    @Override
    public void setT(int t)
    {
        super.setT(t);
        mesh.setT(t);
    }

    @Override
    public Iterator<Point3d> iterator()
    {
        // return a "tweaked" iterator that will skip null entries automatically

        return new Iterator<Point3d>()
        {
            final Iterator<Vertex3D> vertexIterator = mesh.getVertices().iterator();

            Vertex3D next = null;
            boolean nextFetched = false;

            @Override
            public void remove()
            {
                vertexIterator.remove();
            }

            private void fetchNext()
            {
                if (nextFetched)
                    return;

                do
                    next = vertexIterator.next();
                while ((next == null) && vertexIterator.hasNext());

                nextFetched = true;
            }

            @Override
            public Point3d next()
            {
                fetchNext();
                nextFetched = false;

                return next.position;
            }

            @Override
            public boolean hasNext()
            {
                if (!vertexIterator.hasNext())
                    next = null;
                else
                    fetchNext();

                return (next != null);
            }
        };
    }

    void move(ROI boundField, double timeStep)
    {
        final Vector3d force = new Vector3d();
        final double maxDisp = sampling.getValue() * timeStep;
        final Point3d p = new Point3d();

        for (Vertex3D v : mesh.getVertices())
        {
            if (v == null)
                continue;

            final ActiveVertex av = (ActiveVertex) v;

            p.set(v.position);

            // if (Double.isNaN(v.position.x) || Double.isNaN(v.position.y) || Double.isNaN(v.position.z))
            // {
            // System.out.println("oups !");
            // }

            // apply model forces if p lies within the area of interest
            if ((boundField != null) && (p.z >= 0) && (boundField.contains(p.x, p.y, p.z, 0, 0)))
            {
                if (av.volumeConstraint.length() > 0)
                    av.position.add(av.volumeConstraint);

                // if (Double.isNaN(av.position.x) || Double.isNaN(av.position.y) || Double.isNaN(av.position.z))
                // {
                // System.out.println("oups !");
                // }

                force.set(av.imageForces);
                force.add(av.internalForces);
                force.add(av.feedbackForces);

                // if (Double.isNaN(force.x) || Double.isNaN(force.y) || Double.isNaN(force.z))
                // {
                // System.out.println("oups !");
                // }
            }
            else
            {
                // FIXME: better to take all force but just reducing them to avoid big oscillation (Stephane)
                // // force.set(av.imageForces);
                // force.set(av.internalForces);
                // force.scale(0.1);

                force.set(av.imageForces);
                force.add(av.internalForces);
                force.add(av.feedbackForces);
                force.scale(0.1);

                // if (Double.isNaN(force.x) || Double.isNaN(force.y) || Double.isNaN(force.z))
                // {
                // System.out.println("oups !");
                // }
            }

            force.scale(timeStep);

            double disp = force.length();
            // forces cannot be larger than the max authorized displacement (stability condition)
            if (disp > maxDisp)
                force.scale(maxDisp / disp);

            // move the vertex
            v.position.add(force);

            // reset forces
            av.imageForces.set(0, 0, 0);
            av.internalForces.set(0, 0, 0);
            av.feedbackForces.set(0, 0, 0);
            av.volumeConstraint.set(0, 0, 0);

            // if (Double.isNaN(v.position.x) || Double.isNaN(v.position.y) || Double.isNaN(v.position.z))
            // {
            // System.out.println("oups !");
            // }
        }

        updateMetaData();

        // compute some convergence criterion
        if (convergence != null)
            convergence.push(mesh.getNumberOfPoints());
    }

    @Override
    public boolean hasConverged(Operation operation, double epsilon)
    {
        final Double value = convergence.computeCriterion(operation);
        return (value != null) && (value.doubleValue() <= (epsilon / 10d));
    }

    @Override
    protected void updateMetaData()
    {
        super.updateMetaData();

        mesh.roiChanged(true);
    }

    @Override
    public boolean saveToXML(Node node)
    {
        if (!super.saveToXML(node))
            return false;

        return mesh.saveToXML(node);
    }

    @Override
    public boolean loadFromXML(Node node)
    {
        if (!super.loadFromXML(node))
            return false;

        boolean success = mesh.loadFromXML(node);

        mesh.setT(getT());

        return success;
    }

    @Override
    public void reSample(double minFactor, double maxFactor) throws TopologyException
    {
        try
        {
            mesh.resample(sampling.getValue(), 0.4);
        }
        catch (MeshTopologyException e)
        {
            if (e.children == null)
                throw new TopologyException(this, null);

            Mesh3D[] children = new Mesh3D[e.children.length];

            for (int i = 0; i < children.length; i++)
                children[i] = new Mesh3D(sampling, pixelSize, e.children[i], convergence);

            throw new TopologyException(this, children);
        }
    }

    @Override
    public ActiveContour clone()
    {
        return new Mesh3D(this);
    }

    @Override
    protected void addPoint(Point3d p)
    {
        mesh.addVertex(mesh.createVertex(p));
    }

    @Override
    protected ActiveContour[] checkSelfIntersection(double minDistance)
    {
        // TODO
        return null;
    }

    @Override
    public double computeAverageIntensity(Sequence regionData, BooleanMask3D mask) throws TopologyException, UnsupportedOperationException, InterruptedException
    {
        if (mask != null)
        {
            // get mesh boolean mask
            final ROI3DArea localROIMask = mesh.getROIMask();
            final BooleanMask3D localMask = mesh.getMask();
            final List<Integer> zIndexes = new ArrayList<Integer>(mask.mask.keySet());

            for (Integer z : zIndexes)
            {
                // build mask from mesh mask
                final BooleanMask2D localMask2D = localMask.getMask2D(z);
                if (localMask2D != null)
                    mask.mask.put(z, (BooleanMask2D) localMask2D.clone());
            }

            final int c = localROIMask.getC();
            // change to channel 0 as regionData is a single channel image
            localROIMask.setC(0);

            try
            {
                return ROIMeanIntensityDescriptor.computeMeanIntensity(localROIMask, regionData);
            }
            finally
            {
                localROIMask.setC(c);
            }
        }

        return 0d;
    }

    public double computeBackgroundIntensity(Sequence imageData, BooleanMask3D mask)
    {
        Rectangle3D.Integer b3 = mask.bounds;

        // attempt to calculate a localized average outside each contour
        Point3d min = new Point3d(), max = new Point3d();

        boundingBox.getLower(min);
        boundingBox.getUpper(max);

        double zExtent = max.z - min.z;
        int minZ = Math.max(0, (int) Math.round(min.z - zExtent / 2));
        int maxZ = Math.min(b3.sizeZ, (int) Math.round(max.z + zExtent / 2));

        double yExtent = max.y - min.y;
        int minY = Math.max(0, (int) Math.round(min.y - yExtent / 2));
        int maxY = Math.min(b3.sizeY, (int) Math.round(max.y + yExtent / 2));

        double xExtent = max.x - min.x;
        int minX = Math.max(0, (int) Math.round(min.x - xExtent / 2));
        int maxX = Math.min(b3.sizeX, (int) Math.round(max.x + xExtent / 2));

        double outSum = 0, outCpt = 0;
        for (int zSlice = minZ; zSlice < maxZ; zSlice++)
        {
            boolean[] _mask = mask.mask.get(zSlice).mask;
            float[] _data = imageData.getDataXYAsFloat(0, zSlice, 0);

            for (int j = minY; j < maxY; j++)
            {
                int offset = minX + j * b3.sizeX;

                for (int i = minX; i < maxX; i++, offset++)
                {
                    if (!_mask[offset])
                    {
                        outSum += _data[i];
                        outCpt++;
                    }
                }
            }
        }

        return (outCpt == 0) ? 0 : (outSum / outCpt);
    }

    @Override
    public double getDistanceToEdge(Point3d p)
    {
        return mesh.getPenetrationDepth(p);
    }

    @Override
    public ROI toROI()
    {
        return toROI(ROIType.POLYGON, null);
    }

    @Override
    public ROI toROI(ROIType type, Sequence sequence) throws UnsupportedOperationException
    {
        ROI3D roi;

        switch (type)
        {
            case POLYGON:
            {
                roi = mesh.clone();
                break;
            }

            case AREA:
            {
                roi = new ROI3DArea(mesh.getMask());
                break;
            }

            default:
                throw new UnsupportedOperationException("Cannot export a ROI of type: " + type);
        }

        ROIUtil.copyROIProperties(mesh, roi, false);
        roi.setT(t);
        roi.setName(name);

        return roi;
    }

    @Override
    public void toSequence(Sequence output, double value)
    {
        // TODO
    }

    @Override
    protected void updateNormals()
    {
        mesh.updateNormals();
    }

    @Override
    protected void clean()
    {
        isRemoving = true;

        for (Overlay overlay : overlays.values())
            overlay.remove();
        overlays.clear();
    }

    @Override
    public void paint(Graphics2D g, final Sequence sequence, IcyCanvas canvas)
    {
        if (isRemoving)
            return;

        if (overlays.containsKey(canvas))
            return;

        final Overlay overlay = mesh.getOverlay();
        overlay.setCanBeRemoved(true);
        overlay.setName("Active Mesh overlay");
        overlays.put(canvas, overlay);
        sequence.addOverlay(overlay);

        overlay.paint(g, sequence, canvas);
    }
}
