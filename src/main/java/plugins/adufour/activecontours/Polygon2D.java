package plugins.adufour.activecontours;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import icy.canvas.IcyCanvas;
import icy.canvas.IcyCanvas2D;
import icy.gui.frame.progress.AnnounceFrame;
import icy.image.IcyBufferedImage;
import icy.roi.BooleanMask2D;
import icy.roi.BooleanMask3D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.type.collection.CollectionUtil;
import icy.type.collection.array.Array1DUtil;
import icy.type.rectangle.Rectangle3D;
import icy.util.XMLUtil;
import plugins.adufour.activecontours.ActiveContours.ROIType;
import plugins.adufour.activecontours.SlidingWindow.Operation;
import plugins.adufour.morphology.FillHolesInROI;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarDouble;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi2d.ROI2DEllipse;
import plugins.kernel.roi.roi2d.ROI2DPolygon;
import plugins.kernel.roi.roi2d.ROI2DRectangle;
import plugins.kernel.roi.roi2d.ROI2DShape;

public class Polygon2D extends ActiveContour
{
    private static final class Segment implements Iterable<Point3d>
    {
        final ArrayList<Point3d> points;

        Segment(final Point3d head, final Point3d tail)
        {
            points = new ArrayList<Point3d>(2);
            points.add(head);
            points.add(tail);
        }

        final Point3d getHead()
        {
            return points.get(0);
        }

        final Point3d getTail()
        {
            return points.get(points.size() - 1);
        }

        final void addHead(final Point3d p)
        {
            points.add(0, p);
        }

        final void addHead(final Segment s)
        {
            for (int i = 0; i < s.points.size(); i++)
                points.add(i, s.points.get(i));
        }

        final void addTail(final Point3d p)
        {
            points.add(p);
        }

        @Override
        public Iterator<Point3d> iterator()
        {
            return points.iterator();
        }
    }

    private final FillHolesInROI holeFiller = new FillHolesInROI();

    final ArrayList<Point3d> points = new ArrayList<Point3d>();

    Path2D.Double path = new Path2D.Double();

    double cout = 0.0;

    private Vector3d[] modelForces;

    private Vector3d[] contourNormals;

    private Vector3d[] feedbackForces;

    private Vector3d[] volumeConstraintForces;

    /**
     * For XML loading purposes only
     */
    public Polygon2D()
    {
        super();
    }

    protected Polygon2D(final Var<Double> sampling, final SlidingWindow convergenceWindow)
    {
        super(sampling, convergenceWindow);

        setColor(Color.getHSBColor((float) Math.random(), 0.8f, 0.9f));
    }

    /**
     * Creates a clone of the specified contour
     * 
     * @param contour
     */
    public Polygon2D(final Polygon2D contour)
    {
        this(contour.sampling, new SlidingWindow(contour.convergence.getSize()));

        setColor(contour.getColor());
        setZ(contour.getZ());
        setName(contour.getName());

        final int n = contour.points.size();

        points.ensureCapacity(n);
        contourNormals = new Vector3d[n];
        modelForces = new Vector3d[n];
        feedbackForces = new Vector3d[n];
        volumeConstraintForces = new Vector3d[n];

        for (int i = 0; i < n; i++)
        {
            contourNormals[i] = new Vector3d();
            modelForces[i] = new Vector3d();
            feedbackForces[i] = new Vector3d();
            volumeConstraintForces[i] = new Vector3d();
            addPoint(new Point3d(contour.points.get(i)));
        }

        updateMetaData();
        // counterClockWise = contour.counterClockWise;
    }

    /**
     * @param sampling
     * @param convergenceWindow
     * @param roi
     * @throws TopologyException
     *         if the contour cannot created for topological reason (e.g. the roi is too small
     *         w.r.t. the sampling size)
     * @throws InterruptedException
     */
    public Polygon2D(final Var<Double> sampling, final SlidingWindow convergenceWindow, ROI2D roi) throws TopologyException, InterruptedException
    {
        this(sampling, convergenceWindow);

        if (!(roi instanceof ROI2DEllipse) && !(roi instanceof ROI2DRectangle) && !(roi instanceof ROI2DPolygon) && !(roi instanceof ROI2DArea))
        {
            throw new IcyHandledException("Active contours: invalid ROI. Only Rectangle, Ellipse, Polygon and Area are supported");
        }

        setZ(roi.getZ());
        // setName("Contour (" + roi.getName() + ")");
        // better to just keep original name, AC will rename it later
        setName(roi.getName());

        // Don't bother initializing contours that are too small w.r.t. the required sampling
        if (roi.getNumberOfPoints() < sampling.getValue() * 3)
            throw new TopologyException(this, null);

        if (roi instanceof ROI2DArea)
        {
            // dilate ROI
            roi = dilateROI(roi, 1, 1);

            // fill holes first
            final BooleanMask2D mask = roi.getBooleanMask(true);
            if (holeFiller.fillHoles(mask))
                ((ROI2DArea) roi).setAsBooleanMask(mask);

            try
            {
                triangulate((ROI2DArea) roi, sampling.getValue());
                reSample(0.8, 1.4);
                return;
            }
            catch (final TopologyException e)
            {
                final int theZ = roi.getZ();
                final int theT = roi.getT();
                roi = new ROI2DEllipse(roi.getBounds2D());
                roi.setZ(theZ);
                roi.setT(theT);
            }
        }

        if (roi instanceof ROI2DShape)
        {
            // convert the ROI into a linked list of points
            final double[] segment = new double[6];

            final PathIterator pathIterator = ((ROI2DShape) roi).getPathIterator(null, 0.1);

            // first segment is necessarily a "move to" operation

            pathIterator.currentSegment(segment);

            addPoint(new Point3d(segment[0], segment[1], z));

            while (!pathIterator.isDone())
            {
                if (pathIterator.currentSegment(segment) == PathIterator.SEG_LINETO)
                {
                    addPoint(new Point3d(segment[0], segment[1], z));
                }
                pathIterator.next();

                // the final one should be a "close" operation, do nothing
            }
        }

        // if (points.size() <= 4)
        // {
        // // replace by ellipse
        // int z = roi.getZ();
        // int t = roi.getT();
        // roi = new ROI2DEllipse(roi.getBounds2D());
        // roi.setZ(z);
        // roi.setT(t);
        //
        // points.clear();
        // }

        if (getAlgebraicInterior() > 0)
        {
            // Contour is defined in counter-clockwise order
            // => inverse it to clockwise ordering
            Collections.reverse(points);
        }

        reSample(0.8, 1.4);
    }

    @Override
    protected void addPoint(final Point3d p)
    {
        points.add(p);
    }

    /**
     * Checks whether the contour is self-intersecting. Depending on the given parameters, a
     * self-intersection can be considered as a loop or as a contour division.
     * 
     * @param minDistance
     *        the distance threshold between non-neighboring points to detect self-intersection
     * @return <code>null</code> if either no self-intersection is detected or if one of the new
     *         contours is too small, or an array of contours with 0 elements if both contours are
     *         too small, and 2 elements if both contours are viable
     */
    @Override
    protected Polygon2D[] checkSelfIntersection(final double minDistance)
    {
        final double divSensitivity = (divisionSensitivity == null) ? 0d : divisionSensitivity.getValue();
        double divisionDistQ = boundingSphere.getRadius() * 2 * divSensitivity;
        divisionDistQ *= divisionDistQ;
        double minDistanceQ = minDistance;
        minDistanceQ *= minDistanceQ;

        // FIX (Stephane): make contourNormals consistent with 'points'
        final List<Vector3d> contourNormalList = CollectionUtil.asArrayList(contourNormals);

        // FIX (Stephane): we sometime need several iterations to remove complex or multiple loops in contour
        // and we really want to avoid any contour evolution with remaining loop (or it can create chaos !)
        while (true)
        {
            int i = 0, j = 0;
            int n = points.size();
            Point3d p_i = null, p_j = null;
            boolean selfIntersection = false;

            loop:
            for (i = 0; i < n; i++)
            {
                p_i = points.get(i);
                final Vector3d n_i = contourNormalList.get(i);

                for (j = i + 2; j < n - 1; j++)
                {
                    p_j = points.get(j);

                    final double distQ = p_i.distanceSquared(p_j);

                    if (distQ < minDistanceQ)
                    {
                        // local self-intersection
                        // deal with the special case that i and j are 2 points away

                        if (i == 0 && j == n - 2)
                        {
                            n--;
                            points.remove(n);
                            contourNormalList.remove(n);
                            continue;
                        }
                        else if (i == 1 && j == n - 1)
                        {
                            points.remove(0);
                            contourNormalList.remove(0);
                            n--;
                            continue;
                        }
                        else if (j == i + 2)
                        {
                            points.remove(i + 1);
                            contourNormalList.remove(i + 1);
                            n--;
                            continue;
                        }

                        // a real self-intersection is happening
                        selfIntersection = true;
                        break loop;
                    }

                    // self-intersection always involves opposite normals
                    if (n_i.dot(contourNormalList.get(j)) > -0.5)
                        continue;

                    // look for division

                    // what about the intensity profile between v1 and v2?
                    // ROI2DLine line = new ROI2DLine(points.get(i).x, points.get(i).y,
                    // points.get(j).x, points.get(j).y);
                    // int[] linePoints = line.getBooleanMask(true).getPointsAsIntArray();

                    // are points sufficiently close?
                    // => use the bounding radius / 2
                    if (distQ < divisionDistQ)
                    {
                        // are points located "in front" of each other?
                        // j - i ~= n/2 ?
                        // take a loose guess (+/- n/7)
                        if ((j - i) > (2 * n / 5) && (j - i) < (3 * n / 5))
                        {
                            // check the local curvature on each side (4 points away)

                            final Vector3d vi1 = new Vector3d(points.get((i + n - 4) % n));
                            final Vector3d vi2 = new Vector3d(points.get((i + 4) % n));
                            vi1.sub(p_i);
                            vi2.sub(p_i);
                            vi1.normalize();
                            vi2.normalize();
                            vi1.cross(vi1, vi2);
                            // discard small of positive curvatures (i.e. z < 0)
                            if (vi1.z < 0.05)
                                continue;

                            final double vi_lQ = vi1.lengthSquared();
                            // System.out.println(vi1.lengthSquared());
                            final Vector3d vj1 = new Vector3d(points.get((j + n - 4) % n));
                            final Vector3d vj2 = new Vector3d(points.get((j + 4) % n));
                            vj1.sub(p_j);
                            vj2.sub(p_j);
                            vj1.normalize();
                            vj2.normalize();
                            vj1.cross(vj1, vj2);

                            // discard small of positive curvatures (i.e. z < 0)
                            if (vj1.z < 0.05)
                                continue;

                            final double vj_lQ = vj1.lengthSquared();
                            // System.out.println(vj1.lengthSquared());

                            // curvature has to be at a relative minimum
                            if (vi_lQ < 0.5 && vj_lQ < 0.5)
                                continue;

                            // System.out.println(vi_lQ + " | " + vj_lQ);

                            // a real self-intersection is happening
                            selfIntersection = true;
                            break loop;
                        }
                    }

                }
            }

            if (!selfIntersection)
                return null;

            final Point3d center = new Point3d();

            int nPoints = j - i;
            final Polygon2D child1 = new Polygon2D(sampling, new SlidingWindow(this.convergence.getSize()));
            for (int p = 0; p < nPoints; p++)
            {
                final Point3d pp = points.get((p + i) % n);
                center.add(pp);
                child1.addPoint(pp);
            }
            center.scale(1.0 / nPoints);
            child1.setName(getName());
            child1.setX(center.x);
            child1.setY(center.y);
            child1.setZ(center.z);
            child1.setT(getT());

            center.set(0, 0, 0);

            nPoints = i + n - j;
            final Polygon2D child2 = new Polygon2D(sampling, new SlidingWindow(this.convergence.getSize()));
            for (int p = 0, pj = p + j; p < nPoints; p++, pj++)
            {
                final Point3d pp = points.get(pj % n);
                center.add(pp);
                child2.addPoint(pp);
            }
            center.scale(1.0 / nPoints);
            child2.setName(getName());
            child2.setX(center.x);
            child2.setY(center.y);
            child2.setZ(center.z);
            child2.setT(getT());

            final double c1area = child1.getDimension(2);
            final double c2area = child2.getDimension(2);

            // if only one of the two children has a size lower than minArea, then the division
            // should be considered as an artifact loop, the other child thus is the new contour

            if (child1.points.size() < 10 || c1area < (c2area / 8))
            {
                // remove c1 (too small)
                points.clear();
                points.addAll(child2.points);
                continue;
            }

            if (child2.points.size() < 10 || c2area < (c1area / 8))
            {
                // remove c2 (too small)
                points.clear();
                points.addAll(child1.points);
                continue;
            }

            // determine whether the intersection is a loop or a division
            // rationale: check the normal of the two colliding points (i & j)
            // if they point away from the junction => division
            // if they point towards the junction => loop

            final Vector3d n_i = contourNormalList.get(i);
            final Vector3d i_j = new Vector3d(p_j.x - p_i.x, p_j.y - p_i.y, 0);

            // division => keep both then...
            if (n_i.dot(i_j) < 0)
                return new Polygon2D[] {child1, child2};

            // loop => keep only the contour with correct orientation
            // => the contour with a positive algebraic area
            points.clear();

            // c1 is the outer loop => keep it
            if (child1.getAlgebraicInterior() < 0)
                points.addAll(child1.points);
            // c1 is the inner loop => keep c2
            else if (child2.getAlgebraicInterior() < 0)
                points.addAll(child2.points);
        }
    }

    @Override
    protected void clean()
    {
        // nothing to clean (everything should be garbage-collected)
    }

    @Override
    public Polygon2D clone()
    {
        return new Polygon2D(this);
    }

    /**
     * Update the axis constraint force, which adjusts the takes the final forces and normalize them
     * to keep the contour shape along its principal axis <br>
     * WARNING: this method directly update the array of final forces used to displace the contour
     * points. It should be used among the last to keep it most effective
     * 
     * @param weight
     */
    @Override
    void computeAxisForces(final double weight)
    {
        final Vector3d axis = new Vector3d();
        final int s = (int) getDimension(0);

        // Compute the object axis as the vector between the two most distant
        // contour points
        // TODO this is not optimal, geometric moments should be used
        {
            double maxDistSq = 0;
            final Vector3d vec = new Vector3d();

            for (int i = 0; i < s; i++)
            {
                final Point3d vi = points.get(i);

                for (int j = i + 1; j < s; j++)
                {
                    final Point3d vj = points.get(j);

                    vec.sub(vi, vj);
                    final double dSq = vec.lengthSquared();

                    if (dSq > maxDistSq)
                    {
                        maxDistSq = dSq;
                        axis.set(vec);
                    }
                }
            }

            axis.normalize();
        }

        // To drive the contour along the main object axis, each displacement
        // vector is scaled by the scalar product between its normal and the main axis.
        {
            for (int i = 0; i < s; i++)
            {
                final Vector3d normal = contourNormals[i];

                // dot product between normalized vectors ranges from -1 to 1
                final double colinearity = Math.abs(normal.dot(axis)); // now from 0 to 1

                // goal: adjust the minimum using the weight, but keep max to 1
                final double threshold = Math.max(colinearity, 1 - weight);

                if (normal != null)
                    modelForces[i].scale(threshold);
            }
        }
    }

    @Override
    void computeBalloonForces(final double weight)
    {
        final int n = points.size();

        for (int i = 0; i < n; i++)
        {
            final Vector3d force = modelForces[i];

            force.x += weight * contourNormals[i].x;
            force.y += weight * contourNormals[i].y;
        }
    }

    /**
     * Update edge term of the contour evolution according to the image gradient
     * 
     * @param weight
     * @param edge_data
     */
    @Override
    void computeEdgeForces(final Sequence edgeData, final int channel, final double weight)
    {
        final IcyBufferedImage image = edgeData.getImage(0, (int) Math.round(getZ()));
        if (image == null)
            return;

        final Vector3d grad = new Vector3d();

        for (int i = 0; i < points.size(); i++)
        {
            final Point3d p = points.get(i);
            final Vector3d force = modelForces[i];

            // compute the gradient (2nd order)
            final double nextX = getPixelValue(image, p.x + 0.5d, p.y, channel); // getPixelValue(data, width, height, p.x + 0.5, p.y);
            final double prevX = getPixelValue(image, p.x - 0.5d, p.y, channel); // getPixelValue(data, width, height, p.x - 0.5, p.y);
            final double nextY = getPixelValue(image, p.x, p.y + 0.5d, channel); // getPixelValue(data, width, height, p.x, p.y + 0.5);
            final double prevY = getPixelValue(image, p.x, p.y - 0.5d, channel); // getPixelValue(data, width, height, p.x, p.y - 0.5);

            if ((nextX != 0d) && (prevX != 0d) && (nextY != 0d) && (prevY != 0d))
            {
                grad.set(nextX - prevX, nextY - prevY, 0.0);
                grad.scale(weight);
                force.add(grad);
            }
        }
    }

    @Override
    void computeRegionForces(final Sequence imageData, final int channel, double weight, final double sensitivity, final double inAvg, final double outAvg)
    {
        // sensitivity should be high for dim objects, low for bright objects...
        // ... but none of the following options work properly
        // sensitivity *= 1/(1+cin);
        // sensitivity = sensitivity * cin / (0.01 + cout);
        // sensitivity = sensitivity / (2 * Math.max(cout, cin));
        // sensitivity = sensitivity / (Math.log10(cin / cout));

        Point3d p;
        Vector3d force, norm;
        final Vector3d cvms = new Vector3d();
        double val, inDiff, outDiff, forceFactor;
        final int n = points.size();

        // int width = imageData.getWidth();
        // int height = imageData.getHeight();

        weight *= sampling.getValue();

        final int myZ = (int) Math.round(getZ());
        final IcyBufferedImage image = imageData.getImage(0, myZ);
        // float[] _data = imageData.getDataXYAsFloat(0, myZ, channel);
        if (image == null)
            throw new IllegalArgumentException("Contour.getZ() = " + getZ() + "; Stack size = " + imageData.getSizeZ());

        for (int i = 0; i < n; i++)
        {
            p = points.get(i);
            force = modelForces[i];
            norm = contourNormals[i];

            // bounds check
            // if (p.x <= 1 || p.y <= 1 || p.x >= width - 2 || p.y >= height - 2) continue;

            val = getPixelValue(image, p.x, p.y, channel);
            // val = getPixelValue(_data, width, height, p.x, p.y);

            inDiff = val - inAvg;
            inDiff *= inDiff;

            outDiff = val - outAvg;
            outDiff *= outDiff;

            forceFactor = weight * (sensitivity * outDiff) - (inDiff / sensitivity);

            cvms.scale(forceFactor, norm);

            force.add(cvms);
        }

    }

    @Override
    void computeInternalForces(double weight)
    {
        if (feedbackForces == null)
            return;

        final int n = points.size();

        if (n < 3)
            return;

        Vector3d force;
        Point3d prev, curr, next;

        weight /= sampling.getValue();

        // initialize points
        prev = points.get(n - 2);
        curr = points.get(n - 1);
        next = points.get(0);

        // middle points
        for (int i = 0; i < n; i++)
        {
            prev = curr;
            curr = next;
            next = points.get((i + 1) % n);

            force = feedbackForces[i];
            force.x += weight * (prev.x - 2 * curr.x + next.x);
            force.y += weight * (prev.y - 2 * curr.y + next.y);
        }
    }

    @Override
    void computeVolumeConstraint(final double targetVolume, final double weight)
    {
        // 1) compute the difference between target and current volume
        final double volumeDiff = targetVolume - getDimension(2);
        // if (volumeDiff > 0): contour too small, should no longer shrink
        // if (volumeDiff < 0): contour too big, should no longer grow

        final int n = points.size();

        final Vector3d totalForce = new Vector3d();
        for (int i = 0; i < n; i++)
        {
            totalForce.add(feedbackForces[i], modelForces[i]);

            // 2) check whether the final force has same direction as the outer normal
            final double forceNorm = totalForce.dot(contourNormals[i]);

            if (forceNorm * volumeDiff < 0)
            {
                totalForce.set(contourNormals[i]);
                totalForce.scale(volumeDiff * weight / targetVolume);
                volumeConstraintForces[i].set(totalForce);
            }
        }
    }

    /**
     * Computes the feedback forces yielded by the penetration of the current contour into the
     * target contour
     * 
     * @param target
     *        the contour that is being penetrated
     * @return the number of actual point-mesh intersection tests
     */
    @Override
    int computeFeedbackForces(final ActiveContour target)
    {
        final Point3d targetCenter = new Point3d();
        target.boundingSphere.getCenter(targetCenter);

        double targetRadiusSq = target.boundingSphere.getRadius();
        targetRadiusSq *= targetRadiusSq;

        int tests = 0;
        int index = 0;

        for (final Point3d p : points)
        {
            final double distanceSq = p.distanceSquared(targetCenter);

            if (distanceSq < targetRadiusSq)
            {
                final double penetration = target.getDistanceToEdge(p);

                // penetration ?
                if (penetration > 0d)
                {
                    final Vector3d feedbackForce = feedbackForces[index];

                    feedbackForce.scaleAdd(-penetration * 0.2, contourNormals[index], feedbackForce);
                    // reduce model force so feed back force will guide the contour (avoid oscillation)
                    modelForces[index].scale(0.1);

                    //// feedbackForce.scale(-penetration * 0.3, contourNormals[index]);
                    // feedbackForce.scaleAdd(-penetration * 0.5, contourNormals[index], feedbackForce);
                    // modelForces[index].scale(0.05);
                }

                tests++;
            }

            index++;
        }

        return tests;
    }

    private static void createEdge(final ArrayList<Segment> segments, final double xStart, final double yStart, final double xEnd, final double yEnd)
    {
        final double EPSILON = 0.00001;

        final Point3d head = new Point3d(xStart, yStart, 0);
        final Point3d tail = new Point3d(xEnd, yEnd, 0);

        if (segments.size() == 0)
        {
            segments.add(new Segment(head, tail));
            return;
        }

        int insertAtTailOf = -1, insertAtHeadOf = -1;

        for (int i = 0; i < segments.size(); i++)
        {
            if (tail.distance(segments.get(i).getHead()) <= EPSILON)
                insertAtHeadOf = i;
            else if (head.distance(segments.get(i).getTail()) <= EPSILON)
                insertAtTailOf = i;
        }

        if (insertAtTailOf >= 0)
        {
            if (insertAtHeadOf >= 0)
            {
                segments.get(insertAtHeadOf).addHead(segments.get(insertAtTailOf));
                segments.remove(insertAtTailOf);
            }
            else
            {
                segments.get(insertAtTailOf).addTail(tail);
            }
        }
        else if (insertAtHeadOf >= 0)
        {
            segments.get(insertAtHeadOf).addHead(head);
        }
        else
        {
            segments.add(new Segment(head, tail));
        }
    }

    /**
     * Perform a morphological dilation on the specified ROI by the given radius in each dimension
     * 
     * @param roi
     *        the ROI to dilate
     * @param xRadius
     *        the radius in pixels along X
     * @param yRadius
     *        the radius in pixels along X
     * @param zRadius
     *        the radius in pixels along Z (not used if <code>roi</code> is 2D)
     * @return a new, dilated ROI of type "area"
     * @throws InterruptedException
     */
    private static ROI2D dilateROI(final ROI2D roi, final int xRadius, final int yRadius) throws InterruptedException
    {
        final int rx = xRadius, rrx = rx * rx;
        final int ry = yRadius, rry = ry * ry;

        final BooleanMask2D m2 = roi.getBooleanMask(true);
        final ROI2DArea r2 = new ROI2DArea(m2);

        r2.setT(roi.getT());
        r2.setZ(roi.getZ());
        r2.setC(roi.getC());

        r2.beginUpdate();

        for (final Point p : m2.getContourPoints())
        {
            // Brute force
            for (int y = -ry; y <= ry; y++)
                for (int x = -rx; x <= rx; x++)
                    if (x * x / rrx + y * y / rry <= 1.0)
                    {
                        if (!m2.contains(p.x + x, p.y + y))
                            r2.addPoint(p.x + x, p.y + y);
                    }
        }
        r2.endUpdate();

        return r2;
    }

    private static double getPixelValue(final IcyBufferedImage image, final double x, final double y, final int c)
    {
        return image.getDataInterpolated(x, y, c);

        // final int xi = (int) x;
        // final int xip = xi + 1;
        // final int yi = (int) y;
        // final int yip = yi + 1;
        // final int sx = image.getSizeX();
        // final int sy = image.getSizeY();
        //
        // double result = 0d;
        //
        // // at least one pixel inside
        // if ((xi < sx) && (yi < sy) && (xip >= 0) && (yip >= 0))
        // {
        // final double ratioNextX = x - xi;
        // final double ratioCurX = 1d - ratioNextX;
        // final double ratioNextY = y - yi;
        // final double ratioCurY = 1d - ratioNextY;
        //
        // if (yi >= 0)
        // {
        // if (xi >= 0)
        // result += image.getData(xi, yi, c) * (ratioCurX * ratioCurY);
        // if (xip < sx)
        // result += image.getData(xip, yi, c) * (ratioNextX * ratioCurY);
        // }
        // if (yip < sy)
        // {
        // if (xi >= 0)
        // result += image.getData(xi, yip, c) * (ratioCurX * ratioNextY);
        // if (xip < sx)
        // result += image.getData(xip, yip, c) * (ratioNextX * ratioNextY);
        // }
        // }
        //
        // return result;
    }

    // /**
    // * Calculates the 2D image value at the given real coordinates by bilinear interpolation
    // *
    // * @param imageFloat
    // * the image to sample (must be of type {@link DataType#DOUBLE})
    // * @param x
    // * the X-coordinate of the point
    // * @param y
    // * the Y-coordinate of the point
    // * @return the interpolated image value at the given coordinates
    // */
    // private static float getPixelValue(float[] data, int width, int height, double x, double y)
    // {
    // // "center" the coordinates to the center of the pixel
    // x -= 0.5;
    // y -= 0.5;
    //
    // int i = (int) Math.floor(x);
    // int j = (int) Math.floor(y);
    //
    // // if (i <= 0 || i >= width - 1 || j <= 0 || j >= height - 1) return 0f;
    // if (i < 0)
    // i = 0;
    // if (j < 0)
    // j = 0;
    // if (i > width - 2)
    // i = width - 2;
    // if (j > height - 2)
    // j = height - 2;
    //
    // float value = 0;
    //
    // final int offset = i + j * width;
    // final int offset_plus_1 = offset + 1; // saves 1 addition
    //
    // x -= i;
    // y -= j;
    //
    // final double mx = 1 - x;
    // final double my = 1 - y;
    //
    // value += mx * my * data[offset];
    // value += x * my * data[offset_plus_1];
    // value += mx * y * data[offset + width];
    // value += x * y * data[offset_plus_1 + width];
    //
    // return value;
    // }

    /**
     * Computes the algebraic area of the current contour. The returned value is negative if the
     * contour points are order clockwise and positive if ordered counter-clockwise. The contour's
     * surface is just the absolute value of this algebraic surface
     * 
     * @return
     */
    protected double getAlgebraicInterior()
    {
        final int nm1 = points.size() - 1;
        double area = 0;

        // all points but the last
        for (int i = 0; i < nm1; i++)
        {
            final Point3d p1 = points.get(i);
            final Point3d p2 = points.get(i + 1);
            area += (p2.x * p1.y - p1.x * p2.y) * 0.5;
        }

        // last point
        final Point3d p1 = points.get(nm1);
        final Point3d p2 = points.get(0);
        area += (p2.x * p1.y - p1.x * p2.y) * 0.5;

        return area;
    }

    @Override
    public double getDimension(final int order)
    {
        if (points.size() <= 1)
            return 0;

        switch (order)
        {

            case 0: // number of points
            {
                return points.size();
            }

            case 1: // perimeter
            {
                final int size = points.size();

                Point3d p1 = points.get(size - 1);
                Point3d p2 = points.get(0);

                double perimeter = p1.distance(p2);

                for (int i = 0; i < size - 1; i++)
                {
                    // shift pair of points by one index
                    p1 = p2;
                    p2 = points.get(i + 1);
                    perimeter += p1.distance(p2);
                }

                return perimeter;
            }
            case 2: // area
            {
                return Math.abs(getAlgebraicInterior());
            }
            default:
                throw new UnsupportedOperationException("Dimension " + order + " not implemented");
        }
    }

    /**
     * Tests whether the given point is inside the contour, and if so returns the penetration depth
     * of this point. <br>
     * This methods computes the number of intersections between the contour and a semi-infinite
     * line starting from the contour center and passing through the given point. The point is thus
     * considered inside if the number of intersections is odd (Jordan curve theorem).<br>
     * Implementation note: the AWT Line2D class only provides a "segment to segment" intersection
     * test instead of a "semi-infinite line to segment" test, meaning that one must "fake" a
     * semi-infinite line using a big segment. This is done by building a segment originating from
     * the given point and leaving in the opposite direction of the contour center. The full segment
     * can be written in algebraic coordinates as
     * 
     * <pre>
     * [PQ] where Q = P + n * CP
     * </pre>
     * 
     * , where n is chosen arbitrarily large.
     * 
     * @param p
     *        a point to test
     */
    @Override
    public double getDistanceToEdge(final Point3d p)
    {
        final Point3d q = new Point3d(p.x + 10000 * (p.x - x), p.y + 10000 * (p.y - y), 0);

        int nb = 0;
        final int nbPtsM1 = points.size() - 1;
        double dist = 0, minDist = Double.MAX_VALUE;

        // all points but the last
        for (int i = 0; i < nbPtsM1; i++)
        {
            final Point3d p1 = points.get(i);
            final Point3d p2 = points.get(i + 1);

            if (Line2D.linesIntersect(p1.x, p1.y, p2.x, p2.y, p.x, p.y, q.x, q.y))
            {
                nb++;
                dist = Line2D.ptLineDist(p1.x, p1.y, p2.x, p2.y, p.x, p.y);
                if (dist < minDist)
                    minDist = dist;
            }
        }

        // last point
        final Point3d p1 = points.get(nbPtsM1);
        final Point3d p2 = points.get(0);
        if (Line2D.linesIntersect(p1.x, p1.y, p2.x, p2.y, p.x, p.y, q.x, q.y))
        {
            nb++;
            dist = Line2D.ptLineDist(p1.x, p1.y, p2.x, p2.y, p.x, p.y);
            if (dist < minDist)
                minDist = dist;
        }

        // return (nb % 2) == 0;
        return (nb % 2 == 1) ? minDist : 0.0;
    }

    @Override
    public Iterator<Point3d> iterator()
    {
        return points.iterator();
    }

    @Override
    void move(final ROI boundField, final double timeStep)
    {
        final int n = points.size();

        if ((modelForces == null) || (modelForces.length != n))
            return;

        final Vector3d force = new Vector3d();
        final double maxDisp = sampling.getValue() * timeStep;
        final Rectangle2D nearBounds;

        if (boundField != null)
        {
            nearBounds = boundField.getBounds5D().toRectangle2D();
            nearBounds.setRect(nearBounds.getX() + 2d, nearBounds.getY() + 2d, nearBounds.getWidth() - 4d, nearBounds.getHeight() - 4d);
        }
        else
            nearBounds = null;

        for (int index = 0; index < n; index++)
        {
            final Point3d p = points.get(index);

            // apply volume constraint forces to position
            if (volumeConstraintForces[index].length() > 0)
                p.add(volumeConstraintForces[index]);

            // apply model forces if p lies within the area of interest
            // FIXME: it appears to be better to always apply model force but just reducing it (Stephane)
            if ((boundField == null) || boundField.contains(p.x, p.y, 0, 0, 0))
            {
                if ((modelForces[index] != null) && (nearBounds != null))
                {
                    // close from border of "bound field" ? --> reduce model forces
                    if ((p.x < nearBounds.getMinX()) || (p.x > nearBounds.getMaxX()))
                        modelForces[index].scale(0.5);
                    if ((p.y < nearBounds.getMinY()) || (p.y > nearBounds.getMaxY()))
                        modelForces[index].scale(0.5);
                }
            }
            else
            {
                // inhibit model force
                if (modelForces[index] != null)
                    modelForces[index].scale(0);
                // inhibit feedback force
                feedbackForces[index].scale(0);
                // // reduce model force
                // if (modelForces[index] != null)
                // modelForces[index].scale(0.1);
                // // reduce feedback force
                // feedbackForces[index].scale(0.25);
            }

            // set initial model force
            if (modelForces[index] != null)
                force.set(modelForces[index]);
            else
                force.set(0, 0, 0);
            // apply feedback forces
            force.add(feedbackForces[index]);
            // scale by timestep
            force.scale(timeStep);

            final double disp = force.length();
            // limit max displacement
            if (disp > maxDisp)
                force.scale(maxDisp / disp);

            // apply force on position
            p.add(force);

            // reset all forces
            modelForces[index].set(0, 0, 0);
            feedbackForces[index].set(0, 0, 0);
            volumeConstraintForces[index].set(0, 0, 0);
        }

        updateMetaData();

        // compute some convergence criterion
        if (convergence != null)
            convergence.push(getDimension(2));
    }

    @Override
    public boolean hasConverged(final Operation operation, final double epsilon)
    {
        final Double value = convergence.computeCriterion(operation);
        return (value != null) && (value.doubleValue() <= (epsilon / 100));
    }

    AnnounceFrame f = null; // new AnnounceFrame("ready");

    @Override
    public void paint(final Graphics2D g, final Sequence sequence, final IcyCanvas canvas)
    {
        // only paint detections on the current frame
        if (getT() != canvas.getPositionT())
            return;

        if (g != null)
        {
            // 2D viewer
            final Rectangle2D viewBounds = ((IcyCanvas2D) canvas).canvasToImage(canvas.getBounds());
            g.clip(viewBounds);

            final float fontSize = (float) canvas.canvasToImageLogDeltaX(30);
            g.setFont(new Font("Trebuchet MS", Font.BOLD, 10).deriveFont(fontSize));

            final double stroke = Math.max(canvas.canvasToImageLogDeltaX(3), canvas.canvasToImageLogDeltaY(3));

            g.setColor(getColor());

            g.setStroke(new BasicStroke((float) stroke, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);

            synchronized (path)
            {
                g.draw(path);
            }
            // this line displays the average intensity inside the object
            // g.drawString(StringUtil.toString(cin, 2), (float) getX() + 3, (float) getY());
        }
        else
        {
            // TODO other viewers
        }
    }

    int cpt = 0;

    /**
     * Re-samples the Contour according to an 'average distance between points'
     * criterion. This method ensures that the distance between two consecutive
     * points is strictly comprised between a minimum value and a maximum value.
     * In order to avoid oscillatory behavior, 'max' and 'min' should verify the
     * following relations: min &lt; 1, max &gt; 1, 2*min &lt;= max.
     * 
     * @param minFactor
     *        the minimum distance between two points.
     * @param maxFactor
     *        the maximum distance between two points.
     */
    @Override
    public void reSample(final double minFactor, final double maxFactor) throws TopologyException
    {
        if (getDimension(1) < 10)
            throw new TopologyException(this, new Polygon2D[] {});

        final double minLength = sampling.getValue() * minFactor;
        final double maxLength = sampling.getValue() * maxFactor;

        // optimization to avoid multiple points.size() calls (WARNING: n must
        // be updated manually whenever points is changed)
        int n = points.size();

        if (contourNormals == null)
        {
            // first pass: update normals once
            contourNormals = new Vector3d[n];
            for (int i = 0; i < n; i++)
                contourNormals[i] = new Vector3d();

            updateNormals();
        }

        final Polygon2D[] children = cpt % 2 == 0 ? null : checkSelfIntersection(sampling.getValue());
        cpt++;

        if (children != null)
            throw new TopologyException(this, children);

        // update the number of total points
        n = points.size();
        boolean noChange = false;

        int iterCount = 0;
        final int maxIter = n * 10;

        while (noChange == false)
        {
            // Safeguard
            if (++iterCount > maxIter)
            {
                System.err.println("[Active Contours] Warning: hitting safeguard (preventing infinite resampling)");
                break;
            }

            noChange = true;

            // all points but the last
            for (int i = 0; i < n - 1; i++)
            {
                if (n < 4)
                    throw new TopologyException(this, new Polygon2D[] {});

                final Point3d pt1 = points.get(i);
                final Point3d pt2 = points.get(i + 1);

                final double distance = pt1.distance(pt2);

                if (distance < minLength)
                {
                    noChange = false;
                    pt2.set((pt1.x + pt2.x) * 0.5, (pt1.y + pt2.y) * 0.5, (pt1.z + pt2.z) * 0.5);
                    points.remove(i);
                    i--; // comes down to i-1+1 when looping
                    n--;
                }
                else if (distance > maxLength)
                {
                    noChange = false;

                    points.add(i + 1, new Point3d((pt1.x + pt2.x) * 0.5, (pt1.y + pt2.y) * 0.5, (pt1.z + pt2.z) * 0.5));
                    i++; // comes down to i+=2 when looping
                    n++;
                }
            }

            // last point
            final Point3d pt1 = points.get(n - 1);
            final Point3d pt2 = points.get(0);

            if (pt1.distance(pt2) < minLength)
            {
                noChange = false;
                pt2.set((pt1.x + pt2.x) * 0.5, (pt1.y + pt2.y) * 0.5, (pt1.z + pt2.z) * 0.5);
                points.remove(n - 1);
                n--;
            }
            else if (pt1.distance(pt2) > maxLength)
            {
                noChange = false;
                points.add(new Point3d((pt1.x + pt2.x) * 0.5, (pt1.y + pt2.y) * 0.5, (pt1.z + pt2.z) * 0.5));
                n++;
            }
        }

        // re-sampling is done => update internal structures
        final int nbPoints = n;
        if (modelForces == null || modelForces.length != nbPoints)
        {
            modelForces = new Vector3d[nbPoints];
            contourNormals = new Vector3d[nbPoints];
            feedbackForces = new Vector3d[nbPoints];
            volumeConstraintForces = new Vector3d[nbPoints];

            for (int i = 0; i < nbPoints; i++)
            {
                modelForces[i] = new Vector3d();
                contourNormals[i] = new Vector3d();
                feedbackForces[i] = new Vector3d();
                volumeConstraintForces[i] = new Vector3d();
            }
        }

        updateMetaData();
    }

    private void triangulate(final ROI2DArea roi, final double resolution) throws TopologyException, InterruptedException
    {
        final ArrayList<Segment> segments = new ArrayList<Segment>();

        final Rectangle bounds = roi.getBounds();

        final int grid = 1;// Math.max(1, (int) Math.round(resolution));
        final double halfgrid = 0.5 * grid;

        final int cubeWidth = grid;
        final int cubeHeight = grid * bounds.width;
        final int cubeDiag = cubeWidth + cubeHeight;

        final boolean[] mask = roi.getBooleanMask(roi.getBounds(), true);
        // erase first line and first row to ensure closed contours
        java.util.Arrays.fill(mask, 0, bounds.width - 1, false);
        for (int o = 0; o < mask.length; o += bounds.width)
            mask[o] = false;

        for (int j = 0; j < bounds.height; j += grid)
        {
            // check for interruption from time to time as this can be a long process
            if (((j & 0xF) == 0xF) && Thread.interrupted())
                throw new InterruptedException("Polygon2D.triangulate() process interrupted.");

            for (int i = 0, index = j * bounds.width; i < bounds.width; i += grid, index += grid)
            {
                // The image is divided into square cells containing two
                // triangles each:
                //
                // a---b---
                // |../|../
                // |./.|./.
                // |/..|/..
                // c---d---
                //
                // By convention I choose to turn around the object in a
                // clockwise fashion
                // Warning: to ensure connectivity, the objects must NOT touch
                // the image border, strange behavior may occur otherwise

                final boolean a = mask[index];
                final boolean b = (i + grid < bounds.width) && mask[index + cubeWidth];
                final boolean c = (j + grid < bounds.height) && mask[index + cubeHeight];
                final boolean d = (i + grid < bounds.width) && (j + grid < bounds.height) && mask[index + cubeDiag];

                // For each triangle, check for difference between image values
                // to determine the contour location
                // => there are 6 possible combinations in each triangle, that
                // is 12 per cube

                if (a != b)
                {
                    if (b == c) // diagonal edge
                    {
                        if (a == false) // b,c are inside
                        {
                            createEdge(segments, i, j + 0.5, i + halfgrid, j);

                        }
                        else
                        // b,c are outside
                        {
                            createEdge(segments, i + halfgrid, j, i, j + halfgrid);

                        }
                    }
                    else
                    // a = c -> vertical edge
                    {
                        if (a == false) // a,c are outside
                        {
                            createEdge(segments, i + halfgrid, j + halfgrid, i + halfgrid, j);

                        }
                        else
                        // a,c are inside
                        {
                            createEdge(segments, i + halfgrid, j, i + halfgrid, j + halfgrid);

                        }
                    }
                }
                else // a = b -> horizontal edge only if c is different
                if (a != c)
                {
                    if (a == false) // a,b are outside
                    {
                        createEdge(segments, i, j + halfgrid, i + halfgrid, j + halfgrid);

                    }
                    else
                    // a,b are inside
                    {
                        createEdge(segments, i + halfgrid, j + halfgrid, i, j + halfgrid);

                    }
                }

                if (c != d)
                {
                    if (b == c) // diagonal edge
                    {
                        if (c == false) // b,c are outside
                        {
                            createEdge(segments, i + halfgrid, j + grid, i + grid, j + halfgrid);

                        }
                        else
                        // b,c are inside
                        {
                            createEdge(segments, i + grid, j + halfgrid, i + halfgrid, j + grid);

                        }
                    }
                    else
                    // b = d -> vertical edge
                    {
                        if (c == false) // b,d are inside
                        {
                            createEdge(segments, i + halfgrid, j + grid, i + halfgrid, j + halfgrid);

                        }
                        else
                        // b,d are outside
                        {
                            createEdge(segments, i + halfgrid, j + halfgrid, i + halfgrid, j + grid);

                        }
                    }
                }
                else // c = d -> horizontal edge only if b is different
                if (b != c)
                {
                    if (b == false) // c,d are inside
                    {
                        createEdge(segments, i + halfgrid, j + halfgrid, i + grid, j + halfgrid);

                    }
                    else
                    // c,d are outside
                    {
                        createEdge(segments, i + grid, j + halfgrid, i + halfgrid, j + halfgrid);

                    }
                }
            }
        }

        if (segments.size() == 0)
            return;

        for (final Point3d p : segments.get(0))
        {
            p.x += bounds.x;
            p.y += bounds.y;
            p.z = roi.getZ();
            addPoint(p);
        }

        // at this point the triangulated contour has an actual resolution of halfgrid
        // if 2*resolution < desired_resolution, resample() will loop and destroy the contour
        // decimate the contour by a factor 2 recursively until 2*resolution >= desired_resolution

        double current_resolution_doubled = halfgrid * 2;
        while (current_resolution_doubled < resolution * 0.7)
        {
            for (int i = 0; i < points.size(); i++)
                points.remove(i);
            current_resolution_doubled *= 2;
        }
    }

    @Override
    protected void updateNormals()
    {
        final int n = points.size();
        Point3d p1 = points.get(n - 2);
        Point3d p = points.get(n - 1);
        Point3d p2 = points.get(0);

        for (int i = 0; i < n; i++)
        {
            p1 = p;
            p = p2;
            p2 = points.get((i + 1) % n);
            contourNormals[i].normalize(new Vector3d(p2.y - p1.y, p1.x - p2.x, 0));
        }
    }

    @Override
    protected void updateMetaData()
    {
        // int n = points.size();
        // if (modelForces == null || modelForces.length != n) modelForces = new Vector3d[n];
        // if (contourNormals == null || contourNormals.length != n) contourNormals = new
        // Vector3d[n];
        // if (feedbackForces == null || feedbackForces.length != n) feedbackForces = new
        // Vector3d[n];
        // if (volumeConstraintForces == null || volumeConstraintForces.length != n)
        // volumeConstraintForces = new Vector3d[n];

        super.updateMetaData();

        updatePath();
    }

    private void updatePath()
    {
        final Path2D.Double newPath = new Path2D.Double();

        final int nbPoints = points.size();

        if (nbPoints > 0)
        {
            Point3d p = points.get(0);
            newPath.moveTo(p.x, p.y);

            for (int i = 1; i < nbPoints; i++)
            {
                p = points.get(i);
                newPath.lineTo(p.x, p.y);
            }
        }
        newPath.closePath();

        this.path = newPath;
    }

    @Deprecated
    @Override
    public ROI2D toROI() throws InterruptedException
    {
        return toROI(ROIType.AREA, null);
    }

    @Override
    public ROI2D toROI(final ROIType type, final Sequence sequence) throws InterruptedException
    {
        ROI2D roi;

        switch (type)
        {
            case AREA:
            {
                // roi = new ROI2DArea();
                // ((ROI2DArea) roi).addShape(path);
                final List<Point2D> p2d = new ArrayList<Point2D>(points.size());
                for (final Point3d p : this)
                    p2d.add(new Point2D.Double(p.x, p.y));
                roi = (ROI2D) ROIUtil.convertToMask(new ROI2DPolygon(p2d));
                break;
            }

            case POLYGON:
            {
                final List<Point2D> p2d = new ArrayList<Point2D>(points.size());
                for (final Point3d p : this)
                    p2d.add(new Point2D.Double(p.x, p.y));
                roi = new ROI2DPolygon(p2d);
                break;
            }

            default:
                throw new IllegalArgumentException("ROI of type " + type + " cannot be exported yet");
        }

        roi.setT(t);
        roi.setName(name);

        return roi;
    }

    @Override
    public double computeAverageIntensity(final Sequence summedImageData, final BooleanMask3D mask)
    {
        int myZ = (int) z;

        if (myZ == -1 && summedImageData.getSizeZ() == 1)
            myZ = 0;

        // float[] _data = summedImageData.getDataXYAsFloat(0, myZ, 0);
        // if (_data == null)
        // throw new IllegalArgumentException(
        // "Contour.getZ() = " + getZ() + "; Stack size = " + summedImageData.getSizeZ());

        final IcyBufferedImage image = summedImageData.getImage(0, myZ);
        if (image == null)
            throw new IllegalArgumentException("Contour.getZ() = " + getZ() + "; Stack size = " + summedImageData.getSizeZ());

        final boolean[] _mask = (mask == null ? null : mask.mask.get(myZ).mask);

        final int w = summedImageData.getSizeX();
        final int h = summedImageData.getSizeY();
        double sum = 0, count = 0;

        final Point3d minBounds = new Point3d();
        final Point3d maxBounds = new Point3d();
        boundingBox.getLower(minBounds);
        boundingBox.getUpper(maxBounds);

        final int minY = Math.max((int) minBounds.y - 1, 0);
        final int maxY = Math.min((int) maxBounds.y + 1, h);
        final int n = points.size();

        final ArrayList<Integer> crosses = new ArrayList<Integer>(10);
        Point3d p1 = null, p2 = null;

        for (int j = minY; j < maxY; j++)
        {
            crosses.clear();

            p1 = points.get(n - 1);

            for (int p = 0; p < n; p++)
            {
                p2 = points.get(p);

                if (j > Math.min(p1.y, p2.y) && j < Math.max(p1.y, p2.y))
                {
                    int cross = (int) Math.round((p1.x + p2.x) * 0.5);
                    if (cross < 0)
                        cross = 0;
                    else if (cross >= w)
                        cross = w - 1;
                    crosses.add(cross);
                }

                p1 = p2;
            }

            if (crosses.size() == 0 || crosses.size() % 2 == 1)
                continue;

            Collections.sort(crosses);

            final int lineOffset = j * w;
            for (int c = 0; c < crosses.size(); c += 2)
            {
                final int crossIN = crosses.get(c);
                final int crossOUT = crosses.get(c + 1);

                sum -= getPixelValue(image, crossIN, j, 0);
                sum += getPixelValue(image, crossOUT, j, 0);

                count += crossOUT - crossIN;
                if (mask != null)
                {
                    try
                    {
                        Arrays.fill(_mask, lineOffset + crossIN, lineOffset + crossOUT, true);
                    }
                    catch (final ArrayIndexOutOfBoundsException e)
                    {
                        String message = "Image size: " + w + " x " + h + "\n";
                        message += "Line offset: " + lineOffset + "\n";
                        message += "Cross IN: " + crossIN + "\n";
                        message += "Cross OUT: " + crossOUT + "\n";
                        message += "\n" + e.getMessage();
                        throw new RuntimeException(message, e);
                    }
                }
            }
        }

        return (count == 0) ? 0d : sum / count;
    }

    @Override
    public double computeBackgroundIntensity(final Sequence imageData, final BooleanMask3D mask)
    {
        final Rectangle3D.Integer b3 = mask.bounds;

        // attempt to calculate a localised average outside each contour
        final Point3d min = new Point3d(), max = new Point3d();

        boundingBox.getLower(min);
        boundingBox.getUpper(max);

        final double yExtent = max.y - min.y;
        final int minY = Math.max(0, (int) Math.round(min.y - yExtent));
        final int maxY = Math.min(b3.sizeY, (int) Math.round(max.y + yExtent));

        final double xExtent = max.x - min.x;
        final int minX = Math.max(0, (int) Math.round(min.x - xExtent));
        final int maxX = Math.min(b3.sizeX, (int) Math.round(max.x + xExtent));

        double outSum = 0, outCpt = 0;

        final boolean[] _mask = mask.mask.get((int) z).mask;
        final float[] _data = imageData.getDataXYAsFloat(0, (int) z, 0);

        for (int j = minY; j < maxY; j++)
        {
            int offset = minX + j * b3.sizeX;
            for (int i = minX; i < maxX; i++, offset++)
                if (!_mask[offset])
                {
                    outSum += _data[i];
                    outCpt++;
                }
        }

        return outSum / outCpt;
    }

    @Override
    public void toSequence(final Sequence output, final double value)
    {
        final int myZ = (int) Math.round(getZ());
        final int myT = Math.round(getT());

        final Object _mask = output.getDataXY(myT, myZ, 0);

        final int sizeX = output.getWidth();
        final int sizeY = output.getHeight();

        // compute the interior mean intensity
        final Point3d minBounds = new Point3d();
        final Point3d maxBounds = new Point3d();
        boundingBox.getLower(minBounds);
        boundingBox.getUpper(maxBounds);

        final int minX = Math.max((int) minBounds.x - 2, -10);
        final int maxX = Math.min((int) maxBounds.x + 2, sizeX + 10);
        final int minY = Math.max((int) minBounds.y - 1, 0);
        final int maxY = Math.min((int) maxBounds.y + 1, sizeY);

        final int n = points.size();
        Point3d p1 = null, p2 = null;
        final TreeSet<Integer> crosses = new TreeSet<Integer>();

        for (int j = minY; j < maxY; j++)
        {
            final int lineOffset = j * sizeX;
            int offset = lineOffset + (minX < 0 ? 0 : minX);

            crosses.clear();
            crosses.add(minX);

            for (int p = 0; p < n - 1; p++)
            {
                p1 = points.get(p);
                p2 = points.get(p + 1);

                if (j >= Math.min(p1.y, p2.y) && j <= Math.max(p1.y, p2.y))
                {
                    // crosses.add((int) Math.round((p1.x + p2.x) * 0.5));
                    final int cross = (int) Math.round(p1.x + ((j - p1.y) * (p2.x - p1.x) / (p2.y - p1.y)));
                    if (crosses.contains(cross))
                    {
                        crosses.remove(cross);
                        Array1DUtil.setValue(_mask, lineOffset + cross, value);
                    }
                    else
                    {
                        crosses.add(cross);
                    }
                }
            }
            p1 = points.get(0);

            if (j >= Math.min(p1.y, p2.y) && j <= Math.max(p1.y, p2.y))
            {
                // crosses.add((int) Math.round((p1.x + p2.x) * 0.5));
                final int cross = (int) Math.round(p1.x + ((j - p1.y) * (p2.x - p1.x) / (p2.y - p1.y)));
                if (crosses.contains(cross))
                {
                    crosses.remove(cross);
                    Array1DUtil.setValue(_mask, lineOffset + cross, value);
                }
                else
                {
                    crosses.add(cross);
                }
            }

            crosses.add(maxX);

            final int nC = crosses.size();

            if (nC > 2)
            {
                boolean in = false;

                final Iterator<Integer> it = crosses.iterator();
                int start = it.next();
                if (start < 0)
                    start = 0;

                while (it.hasNext())
                {
                    int end = it.next();
                    if (end > sizeX)
                        end = sizeX;

                    if (in)
                    {
                        for (int i = start; i < end; i++, offset++)
                        {
                            if (start < 0 || end >= sizeY)
                                continue;
                            
                            Array1DUtil.setValue(_mask, offset, value);
                        }
                    }
                    else
                        offset += end - start;

                    start = end;
                    in = !in;
                }
            }
        }
    }

    /**
     * Perform a raster scan on the contour, and calculate various information based on the
     * specified parameters. The raster scan algorithm is a 2D version of the line-scan algorithm
     * developed in: <i>Dufour et al., 3D active meshes: fast discrete deformable models for cell
     * tracking in 3D time-lapse microscopy. IEEE Transactions on Image Processing 20, 2011</i>
     * 
     * @param updateLocalMask
     *        indicates whether the local boolean mask should be updated (this is used to get a
     *        {@link BooleanMask3D} version of this mesh)
     * @param imageData
     *        (set to <code>null</code> if not needed) a sequence that will be used to compute
     *        the average intensity inside the mesh (note that the T and C have to be different
     *        than <code>-1</code> if the sequence has more than 1 time point and 1 channel)
     * @param averageIntensity
     *        (only used if <code>imageData</code> is provided) a variable that will be hold the
     *        average image intensity inside the contour after the scan is complete
     */
    public void rasterScan(final boolean updateLocalMask, final Sequence imageData, final VarDouble averageIntensity, final BooleanMask3D imageMask)
    {

    }

    @Override
    public boolean loadFromXML(final Node node)
    {
        if (!super.loadFromXML(node))
            return false;

        final Element xmlElement = XMLUtil.getElement(node, "Contour");

        if (xmlElement == null)
            return false;

        for (final Element ptElem : XMLUtil.getElements(xmlElement))
        {
            final double xPt = XMLUtil.getAttributeDoubleValue(ptElem, "x", Double.NaN);
            final double yPt = XMLUtil.getAttributeDoubleValue(ptElem, "y", Double.NaN);
            if (Double.isNaN(xPt) || Double.isNaN(yPt))
                return false;
            points.add(new Point3d(xPt, yPt, 0));
        }

        updatePath();

        return true;
    }

    @Override
    public boolean saveToXML(final Node node)
    {
        if (!super.saveToXML(node))
            return false;

        final Element xmlElement = XMLUtil.addElement(node, "Contour");

        for (final Point3d pt : points)
        {
            final Element xmlPt = XMLUtil.addElement(xmlElement, "Point");
            XMLUtil.setAttributeDoubleValue(xmlPt, "x", pt.x);
            XMLUtil.setAttributeDoubleValue(xmlPt, "y", pt.y);
        }

        return true;
    }
}
