package plugins.adufour.activecontours;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackSegment;

public class ReSampler implements Callable<Boolean>
{
    private final TrackGroup trackGroup;
    private final ActiveContour contour;
    private final Set<ActiveContour> allContours;
    private final Set<ActiveContour> evolvingContours;

    ReSampler(TrackGroup trackGroup, ActiveContour contour, HashSet<ActiveContour> evolvingContours,
            HashSet<ActiveContour> allContours)
    {
        this.trackGroup = trackGroup;
        this.contour = contour;
        this.allContours = allContours;
        this.evolvingContours = evolvingContours;
    }

    public Boolean call()
    {
        boolean change = false;

        try
        {
            contour.reSample(0.6, 1.4);
        }
        catch (Exception e)
        {
            change = true;

            // the contour is either dividing or vanishing
            contour.clean();

            // 1) remove it from the list of contours
            allContours.remove(contour);
            evolvingContours.remove(contour);

            // 2) find the corresponding segment
            TrackSegment currentSegment = null;

            // this is a thread-safe version of TrackGroup.getTrackSegmentWithDetection(Detection)

            synchronized (trackGroup)
            {
                for (TrackSegment track : trackGroup.getTrackSegmentList())
                {
                    if (track == null)
                        continue; // FIXME Fabrice: how could this happen?

                    if (track.containsDetection(contour))
                    {
                        currentSegment = track;
                        break;
                    }
                }

                if (currentSegment != null)
                {
                    currentSegment.removeDetection(contour);

                    if (currentSegment.getDetectionList().size() == 0)
                    {
                        // the current contour is the only detection in this segment
                        // => remove the whole segment
                        trackGroup.removeTrackSegment(currentSegment);
                        currentSegment = null;
                    }
                }

                // not expected exception ?
                if (!(e instanceof TopologyException))
                {
                    // show it in log
                    e.printStackTrace();
                    return Boolean.valueOf(change);
                }

                // 3) Deal with the children
                ActiveContour[] children = ((TopologyException) e).children;

                if (children == null)
                    return Boolean.valueOf(change);

                for (ActiveContour child : children)
                {
                    child.setT(contour.getT());
                    allContours.add(child);
                    evolvingContours.add(child);

                    // create the new track segment with the child contour
                    TrackSegment childSegment = new TrackSegment();
                    childSegment.addDetection(child);

                    synchronized (trackGroup)
                    {
                        trackGroup.addTrackSegment(childSegment);
                    }

                    if (currentSegment != null)
                        currentSegment.addNext(childSegment);
                }
            }
        }

        return Boolean.valueOf(change);
    }
}